# Bible Verse

Displays a selected Bible verse, or verses, from the KJV.
You can change the Bible verse by pressing spacebar or by left clicking on the page.

#### Change Verse Order

By default the verse order is set to *random*. This can be changed to *sequential* using the URL parameter "order=sequential."

    bible-verse/?order=sequential

#### Red Letter

By default the words spoken by Jesus Christ are red. This feature can be disabled using the URL parameter "red-letter=off."

    bible-verse/?red-letter=off

#### Clone Repo

    git clone git@gitlab.com:robertmermet/bible-verse.git

#### View Demo

[robertmermet.com/projects/bible-verse](http://robertmermet.com/projects/bible-verse)