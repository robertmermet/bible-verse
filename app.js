window.addEventListener('load', function load() {
	window.removeEventListener('load', load, false);

	const BIBLE_VERSES = [
			// Genesis
			{
				v: 'Genesis 1:1',
				t: 'In the beginning God created the heaven and the earth.'
			},
			{
				v: 'Genesis 1:3',
				t: 'And God said, Let there be light: and there was light.'
			},
			{
				v: 'Genesis 1:26',
				t: 'And God said, Let us make man in our image, after our likeness: and let them have dominion over the fish of the sea, and over the fowl of the air, and over the cattle, and over all the earth, and over every creeping thing that creepeth upon the earth.'
			},
			{
				v: 'Genesis 1:27',
				t: 'So God created man in his <em>own</em> image, in the image of God created he him; male and female created he them.'
			},
			{
				v: 'Genesis 2:3',
				t: 'And God blessed the seventh day, and sanctified it: because that in it he had rested from all his work which God created and made.'
			},
			{
				v: 'Genesis 2:23-24',
				t: 'And Adam said, This <em>is</em> now bone of my bones, and flesh of my flesh: she shall be called Woman, because she was taken out of Man. Therefore shall a man leave his father and his mother, and shall cleave unto his wife: and they shall be one flesh.'
			},
			{
				v: 'Genesis 3:19',
				t: 'In the sweat of thy face shalt thou eat bread, till thou return unto the ground; for out of it wast thou taken: for dust thou <em>art</em>, and unto dust shalt thou return.'
			},
			// Exodus
			{
				v: 'Exodus 20:3',
				t: 'Thou shalt have no other gods before me.'
			},
			{
				v: 'Exodus 20:4',
				t: 'Thou shalt not make unto thee any graven image, or any likeness <em>of any thing</em> that <em>is</em> in heaven above, or that <em>is</em> in the earth beneath, or that <em>is</em> in the water under the earth.'
			},
			{
				v: 'Exodus 20:7',
				t: 'Thou shalt not take the name of the <span class="small-caps">Lord</span> thy God in vain; for the <span class="small-caps">Lord</span> will not hold him guiltless that taketh his name in vain.'
			},
			{
				v: 'Exodus 20:8-10',
				t: 'Remember the sabbath day, to keep it holy. Six days shalt thou labour, and do all thy work: But the seventh day <em>is</em> the sabbath of the <span class="small-caps">Lord</span> thy God: <em>in it</em> thou shalt not do any work, thou, nor thy son, nor thy daughter, thy manservant, nor thy maidservant, nor thy cattle, nor thy stranger that <em>is</em> within thy gates:'
			},
			{
				v: 'Exodus 20:12',
				t: 'Honour thy father and thy mother: that thy days may be long upon the land which the <span class="small-caps">Lord</span> thy God giveth thee.'
			},
			{
				v: 'Exodus 20:13',
				t: 'Thou shalt not kill.'
			},
			{
				v: 'Exodus 20:14',
				t: 'Thou shalt not commit adultery.'
			},
			{
				v: 'Exodus 20:15',
				t: 'Thou shalt not steal.'
			},
			{
				v: 'Exodus 20:16',
				t: 'Thou shalt not bear false witness against thy neighbour.'
			},
			{
				v: 'Exodus 20:17',
				t: 'Thou shalt not covet thy neighbour\'s house, thou shalt not covet thy neighbour\'s wife, nor his manservant, nor his maidservant, nor his ox, nor his ass, nor any thing that <em>is</em> thy neighbour\'s.'
			},
			{
				v: 'Exodus 22:18',
				t: 'Thou shalt not suffer a witch to live.'
			},
			{
				v: 'Exodus 33:19',
				t: 'And he said, I will make all my goodness pass before thee, and I will proclaim the name of the <span class="small-caps">Lord</span> before thee; and will be gracious to whom I will be gracious, and will shew mercy on whom I will shew mercy.'
			},
			// Leviticus
			{
				v: 'Leviticus 19:18',
				t: 'Thou shalt not avenge, nor bear any grudge against the children of thy people, but thou shalt love thy neighbour as thyself: I <em>am</em> the <span class="small-caps">Lord</span>.'
			},
			{
				v: 'Leviticus 11:44',
				t: 'For I <em>am</em> the <span class="small-caps">Lord</span> your God: ye shall therefore sanctify yourselves, and ye shall be holy; for I <em>am</em> holy: neither shall ye defile yourselves with any manner of creeping thing that creepeth upon the earth.'
			},
			// Numbers
			{
				v: 'Numbers 6:24-26',
				t: 'The <span class="small-caps">Lord</span> bless thee, and keep thee: The <span class="small-caps">Lord</span> make his face shine upon thee, and be gracious unto thee: The <span class="small-caps">Lord</span> lift up his countenance upon thee, and give thee peace.'
			},
			{
				v: 'Numbers 14:18',
				t: 'The <span class="small-caps">Lord</span> <em>is</em> longsuffering, and of great mercy, forgiving iniquity and transgression, and by no means clearing <em>the guilty</em>, visiting the iniquity of the fathers upon the children unto the third and fourth <em>generation</em>.'
			},
			// Deuteronomy
			{
				v: 'Deuteronomy 4:2',
				t: 'Ye shall not add unto the word which I command you, neither shall ye diminish <em>ought</em> from it, that ye may keep the commandments of the <span class="small-caps">Lord</span> your God which I command you.'
			},
			{
				v: 'Deuteronomy 5:9-10',
				t: 'Thou shalt not bow down thyself unto them, nor serve them: for I the <span class="small-caps">Lord</span> thy God <em>am</em> a jealous God, visiting the iniquity of the fathers upon the children unto the third and fourth <em>generation</em> of them that hate me, And shewing mercy unto thousands of them that love me and keep my commandments.'
			},
			{
				v: 'Deuteronomy 6:4-5',
				t: 'Hear, O Israel: The <span class="small-caps">Lord</span> our God <em>is</em> one <span class="small-caps">Lord</span>: And thou shalt love the <span class="small-caps">Lord</span> thy God with all thine heart, and with all thy soul, and with all thy might.'
			},
			{
				v: 'Deuteronomy 6:13',
				t: 'Thou shalt fear the <span class="small-caps">Lord</span> thy God, and serve him, and shalt swear by his name.'
			},
			{
				v: 'Deuteronomy 6:17',
				t: 'Ye shall diligently keep the commandments of the <span class="small-caps">Lord</span> your God, and his testimonies, and his statutes, which he hath commanded thee.'
			},
			{
				v: 'Deuteronomy 8:3',
				t: 'And he humbled thee, and suffered thee to hunger, and fed thee with manna, which thou knewest not, neither did thy fathers know; that he might make thee know that man doth not live by bread only, but by every <em>word</em> that proceedeth out of the mouth of the <span class="small-caps">Lord</span> doth man live.'
			},
			{
				v: 'Deuteronomy 10:12-13',
				t: 'And now, Israel, what doth the <span class="small-caps">Lord</span> thy God require of thee, but to fear the <span class="small-caps">Lord</span> thy God, to walk in all his ways, and to love him, and to serve the <span class="small-caps">Lord</span> thy God with all thy heart and with all thy soul, To keep the commandments of the <span class="small-caps">Lord</span>, and his statutes, which I command thee this day for thy good?'
			},
			{
				v: 'Deuteronomy 16:21-22',
				t: 'Thou shalt not plant thee a grove of any trees near unto the altar of the <span class="small-caps">Lord</span> thy God, which thou shalt make thee. Neither shalt thou set thee up <em>any</em> image; which the <span class="small-caps">Lord</span> thy God hateth.'
			},
			{
				v: 'Deuteronomy 18:10',
				t: 'There shall not be found among you <em>any one</em> that maketh his son or his daughter to pass through the fire, <em>or</em> that useth divination, <em>or</em> an observer of times, or an enchanter, or a witch.'
			},
			{
				v: 'Deuteronomy 22:5',
				t: 'The woman shall not wear that which pertaineth unto a man, neither shall a man put on a woman\'s garment: for all that do so <em>are</em> abomination unto the <span class="small-caps">Lord</span> thy God.'
			},
			{
				v: 'Deuteronomy 27:10',
				t: 'Thou shalt therefore obey the voice of the <span class="small-caps">Lord</span> thy God, and do his commandments and his statutes, which I command thee this day.'
			},
			{
				v: 'Deuteronomy 29:29',
				t: 'The secret <em>things belong</em> unto the <span class="small-caps">Lord</span> our God: but those <em>things which are</em> revealed <em>belong</em> unto us and to our children for ever, that <em>we</em> may do all the words of this law.'
			},
			{
				v: 'Deuteronomy 31:6',
				t: 'Be strong and of a good courage, fear not, nor be afraid of them: for the <span class="small-caps">Lord</span> thy God, he <em>it is</em> that doth go with thee; he will not fail thee, nor forsake thee.'
			},
			// Joshua
			{
				v: 'Joshua 1:9',
				t: 'Have not I commanded thee? Be strong and of a good courage; be not afraid, neither be thou dismayed: for the <span class="small-caps">Lord</span> thy God <em>is</em> with thee whithersoever thou goest.'
			},
			{
				v: 'Joshua 24:15',
				t: 'And if it seem evil unto you to serve the <span class="small-caps">Lord</span>, choose you this day whom ye will serve; whether the gods which your fathers served that <em>were</em> on the other side of the flood, or the gods of the Amorites, in whose land ye dwell: but as for me and my house, we will serve the <span class="small-caps">Lord</span>.'
			},
			// 1 Samuel
			{
				v: '1 Samuel 2:2',
				t: '<em>There is</em> none holy as the <span class="small-caps">Lord</span>: for <em>there is</em> none beside thee: neither <em>is there</em> any rock like our God.'
			},
			// 2 Samuel
			{
				v: '2 Samuel 7:22',
				t: 'Wherefore thou art great, O <span class="small-caps">Lord</span> God: for <em>there is</em> none like thee, neither <em>is there any</em> God beside thee, according to all that we have heard with our ears.'
			},
			{
				v: '2 Samuel 22:3',
				t: 'The God of my rock; in him will I trust: <em>he is</em> my shield, and the horn of my salvation, my high tower, and my refuge, my saviour; thou savest me from violence.'
			},
			// 1 Chronicles
			{
				v: '1 Chronicles 16:11',
				t: 'Seek the <span class="small-caps">Lord</span> and his strength, seek his face continually.'
			},
			{
				v: '1 Chronicles 29:11',
				t: 'Thine, O <span class="small-caps">Lord</span>, <em>is</em> the greatness, and the power, and the glory, and the victory, and the majesty: for all <em>that is</em> in the heaven and in the earth <em>is thine</em>; thine <em>is</em> the kingdom, O <span class="small-caps">Lord</span>, and thou art exalted as head above all.'
			},
			// 2 Chronicles
			{
				v: '2 Chronicles 7:14',
				t: 'If my people, which are called by my name, shall humble themselves, and pray, and seek my face, and turn from their wicked ways; then will I hear from heaven, and will forgive their sin, and will heal their land.'
			},
			{
				v: '2 Chronicles 15:7',
				t: 'Be ye strong therefore, and let not your hands be weak: for your work shall be rewarded.'
			},
			{
				v: '2 Chronicles 20:15',
				t: 'And he said, Hearken ye, all Judah, and ye inhabitants of Jerusalem, and thou king Jehoshaphat, Thus saith the <span class="small-caps">Lord</span> unto you, Be not afraid nor dismayed by reason of this great multitude; for the battle <em>is</em> not yours, but God\'s.'
			},
			// Job
			{
				v: 'Job 1:20-22',
				t: 'Then Job arose, and rent his mantle, and shaved his head, and fell down upon the ground, and worshipped, And said, Naked came I out of my mother\'s womb, and naked shall I return thither: the <span class="small-caps">Lord</span> gave, and the <span class="small-caps">Lord</span> hath taken away; blessed be the name of the <span class="small-caps">Lord</span>. In all this Job sinned not, nor charged God foolishly.'
			},
			// Psalms
			{
				v: 'Psalm 1:1',
				t: 'Blessed <em>is</em> the man that walketh not in the counsel of the ungodly, nor standeth in the way of sinners, nor sitteth in the seat of the scornful.'
			},
			{
				v: 'Psalm 2:2-3',
				t: 'The kings of the earth set themselves, and the rulers take counsel together, against the <span class="small-caps">Lord</span>, and against his anointed, <em>saying</em>, Let us break their bands asunder, and cast away their cords from us.'
			},
			{
				v: 'Psalm 9:1',
				t: 'I will praise <em>thee</em>, O <span class="small-caps">Lord</span>, with my whole heart; I will shew forth all thy marvellous works.'
			},
			{
				v: 'Psalm 9:10',
				t: 'And they that know thy name will put their trust in thee: for thou, <span class="small-caps">Lord</span>, hast not forsaken them that seek thee.'
			},
			{
				v: 'Psalm 12:6-7',
				t: 'The words of the <span class="small-caps">Lord</span> <em>are</em> pure words: <em>as</em> silver tried in a furnace of earth, purified seven times.'
			},
			{
				v: 'Psalm 14:1',
				t: 'The fool hath said in his heart, <em>There is</em> no God. They are corrupt, they have done abominable works, <em>there is</em> none that doeth good.'
			},
			{
				v: 'Psalm 16:8',
				t: 'I have set the <span class="small-caps">Lord</span> always before me: because <em>he is</em> at my right hand, I shall not be moved.'
			},
			{
				v: 'Psalm 17:8-9',
				t: 'Keep me as the apple of the eye, hide me under the shadow of thy wings, From the wicked that oppress me, <em>from</em> my deadly enemies, <em>who</em> compass me about.'
			},
			{
				v: 'Psalm 18:46',
				t: 'The <span class="small-caps">Lord</span> liveth; and blessed <em>be</em> my rock; and let the God of my salvation be exalted.'
			},
			{
				v: 'Psalm 19:1',
				t: 'The heavens declare the glory of God; and the firmament sheweth his handywork.'
			},
			{
				v: 'Psalm 19:7',
				t: 'The law of the <span class="small-caps">Lord</span> <em>is</em> perfect, converting the soul: the testimony of the <span class="small-caps">Lord</span> <em>is</em> sure, making wise the simple.'
			},
			{
				v: 'Psalm 21:13',
				t: 'Be thou exalted, <span class="small-caps">Lord</span>, in thine own strength: <em>so</em> will we sing and praise thy power.'
			},
			{
				v: 'Psalm 23:1',
				t: 'The <span class="small-caps">Lord</span> <em>is</em> my shepherd; I shall not want.'
			},
			{
				v: 'Psalm 23:3',
				t: 'He restoreth my soul: he leadeth me in the paths of righteousness for his name\'s sake.'
			},
			{
				v: 'Psalm 23:4',
				t: 'Yea, though I walk through the valley of the shadow of death, I will fear no evil: for thou <em>art</em> with me; thy rod and thy staff they comfort me.'
			},
			{
				v: 'Psalm 25:8-9',
				t: 'Good and upright <em>is</em> the <span class="small-caps">Lord</span>: therefore will he teach sinners in the way. The meek will he guide in judgment: and the meek will he teach his way.'
			},
			{
				v: 'Psalm 27:1',
				t: 'The <span class="small-caps">Lord</span> <em>is</em> my light and my salvation; whom shall I fear? the <span class="small-caps">Lord</span> <em>is</em> the strength of my life; of whom shall I be afraid?'
			},
			{
				v: 'Psalm 29:11',
				t: 'The <span class="small-caps">Lord</span> will give strength unto his people; the <span class="small-caps">Lord</span> will bless his people with peace.'
			},
			{
				v: 'Psalm 31:14-15',
				t: 'But I trusted in thee, O <span class="small-caps">Lord</span>: I said, Thou <em>art</em> my God. My times <em>are</em> in thy hand: deliver me from the hand of mine enemies, and from them that persecute me.'
			},
			{
				v: 'Psalm 32:5',
				t: 'I acknowledged my sin unto thee, and mine iniquity have I not hid. I said, I will confess my transgressions unto the <span class="small-caps">Lord</span>; and thou forgavest the iniquity of my sin. Selah.'
			},
			{
				v: 'Psalm 34:4',
				t: 'I sought the <span class="small-caps">Lord</span>, and he heard me, and delivered me from all my fears.'
			},
			{
				v: 'Psalm 34:18',
				t: 'The <span class="small-caps">Lord</span> <em>is</em> nigh unto them that are of a broken heart; and saveth such as be of a contrite spirit.'
			},
			{
				v: 'Psalm 34:19',
				t: 'Many <em>are</em> the afflictions of the righteous: but the <span class="small-caps">Lord</span> delivereth him out of them all.'
			},
			{
				v: 'Psalm 37:28',
				t: 'For the <span class="small-caps">Lord</span> loveth judgment, and forsaketh not his saints; they are preserved for ever: but the seed of the wicked shall be cut off.'
			},
			{
				v: 'Psalm 37:39',
				t: 'But the salvation of the righteous <em>is</em> of the LORD: <em>he is</em> their strength in the time of trouble.'
			},
			{
				v: 'Psalm 38:18',
				t: 'For I will declare mine iniquity; I will be sorry for my sin.'
			},
			{
				v: 'Psalm 41:1',
				t: 'Blessed <em>is</em> he that considereth the poor: the <span class="small-caps">Lord</span> will deliver him in time of trouble.'
			},
			{
				v: 'Psalm 46:10',
				t: 'Be still, and know that I <em>am</em> God: I will be exalted among the heathen, I will be exalted in the earth.'
			},
			{
				v: 'Psalm 50:15',
				t: 'And call upon me in the day of trouble: I will deliver thee, and thou shalt glorify me.'
			},
			{
				v: 'Psalm 51:2-3',
				t: 'Wash me throughly from mine iniquity, and cleanse me from my sin. For I acknowledge my transgressions: and my sin <em>is</em> ever before me.'
			},
			{
				v: 'Psalm 55:16',
				t: 'As for me, I will call upon God; and the <span class="small-caps">Lord</span> shall save me.'
			},
			{
				v: 'Psalm 55:17',
				t: 'Evening, and morning, and at noon, will I pray, and cry aloud: and he shall hear my voice.'
			},
			{
				v: 'Psalm 55:22',
				t: 'Cast thy burden upon the <span class="small-caps">Lord</span>, and he shall sustain thee: he shall never suffer the righteous to be moved.'
			},
			{
				v: 'Psalm 57:5',
				t: 'Be thou exalted, O God, above the heavens; <em>let</em> thy glory <em>be</em> above all the earth.'
			},
			{
				v: 'Psalm 62:8',
				t: 'Trust in him at all times; <em>ye</em> people, pour out your heart before him: God <em>is</em> a refuge for us. Selah.'
			},
			{
				v: 'Psalm 69:30',
				t: 'I will praise the name of God with a song, and will magnify him with thanksgiving.'
			},
			{
				v: 'Psalm 71:1',
				t: 'In thee, O <span class="small-caps">Lord</span>, do I put my trust: let me never be put to confusion.'
			},
			{
				v: 'Psalm 73:26',
				t: 'My flesh and my heart faileth: <em>but</em> God <em>is</em> the strength of my heart, and my portion for ever.'
			},
			{
				v: 'Psalm 86:5',
				t: 'For thou, Lord, <em>art</em> good, and ready to forgive; and plenteous in mercy unto all them that call upon thee.'
			},
			{
				v: 'Psalm 91:1',
				t: 'He that dwelleth in the secret place of the most High shall abide under the shadow of the Almighty.'
			},
			{
				v: 'Psalm 94:1-2',
				t: 'O <span class="small-caps">Lord</span> God, to whom vengeance belongeth; O God, to whom vengeance belongeth, shew thyself. Lift up thyself, thou judge of the earth: render a reward to the proud.'
			},
			{
				v: 'Psalm 98:4',
				t: 'Make a joyful noise unto the <span class="small-caps">Lord</span>, all the earth: make a loud noise, and rejoice, and sing praise.'
			},
			{
				v: 'Psalm 99:9',
				t: 'Exalt the <span class="small-caps">Lord</span> our God, and worship at his holy hill; for the <span class="small-caps">Lord</span> our God <em>is</em> holy.'
			},
			{
				v: 'Psalm 111:10',
				t: 'The fear of the <span class="small-caps">Lord</span> is the beginning of wisdom: a good understanding have all they that do <em>his commandments</em>: his praise endureth for ever.'
			},
			{
				v: 'Psalm 112:7',
				t: 'He shall not be afraid of evil tidings: his heart is fixed, trusting in the <span class="small-caps">Lord</span>.'
			},
			{
				v: 'Psalm 113:3',
				t: 'From the rising of the sun unto the going down of the same the <span class="small-caps">Lord</span>\'s name <em>is</em> to be praised.'
			},
			{
				v: 'Psalm 118:8',
				t: '<em>It is</em> better to trust in the <span class="small-caps">Lord</span> than to put confidence in man.'
			},
			{
				v: 'Psalm 118:22',
				t: 'The stone <em>which</em> the builders refused is become the head <em>stone</em> of the corner.'
			},
			{
				v: 'Psalm 119:11',
				t: 'Thy word have I hid in mine heart, that I might not sin against thee.'
			},
			{
				v: 'Psalm 119:18',
				t: 'Open thou mine eyes, that I may behold wondrous things out of thy law.'
			},
			{
				v: 'Psalm 119:30',
				t: 'I have chosen the way of truth: thy judgments have I laid <em>before me</em>.'
			},
			{
				v: 'Psalm 119:71',
				t: '<em>It is</em> good for me that I have been afflicted; that I might learn thy statutes.'
			},
			{
				v: 'Psalm 119:89',
				t: 'For ever, O <span class="small-caps">Lord</span>, thy word is settled in heaven.'
			},
			{
				v: 'Psalm 119:97-99',
				t: 'O how love I thy law! it <em>is</em> my meditation all the day. Thou through thy commandments hast made me wiser than mine enemies: for they <em>are</em> ever with me. I have more understanding than all my teachers: for thy testimonies <em>are</em> my meditation.'
			},
			{
				v: 'Psalm 119:160',
				t: 'Thy word <em>is</em> true <em>from</em> the beginning: and every one of thy righteous judgments <em>endureth</em> for ever.'
			},
			{
				v: 'Psalm 127:1',
				t: 'Except the <span class="small-caps">Lord</span> build the house, they labour in vain that build it: except the Lord keep the city, the watchman waketh <em>but</em> in vain.'
			},
			{
				v: 'Psalm 127:3',
				t: 'Lo, children <em>are</em> an heritage of the <span class="small-caps">Lord</span>: <em>and</em> the fruit of the womb <em>is his</em> reward.'
			},
			{
				v: 'Psalm 130:1-4',
				t: 'Out of the depths have I cried unto thee, O <span class="small-caps">Lord</span>. Lord, hear my voice: let thine ears be attentive to the voice of my supplications. If thou, <span class="small-caps">Lord</span>, shouldest mark iniquities, O Lord, who shall stand? But <em>there is</em> forgiveness with thee, that thou mayest be feared.'
			},
			{
				v: 'Psalm 136:1',
				t: 'O give thanks unto the <span class="small-caps">Lord</span>; for <em>he is</em> good: for his mercy <em>endureth</em> for ever.'
			},
			{
				v: 'Psalm 143:10',
				t: 'Teach me to do thy will; for thou <em>art</em> my God: thy spirit <em>is</em> good; lead me into the land of uprightness.'
			},
			{
				v: 'Psalm 145:18',
				t: 'The <span class="small-caps">Lord</span> <em>is</em> nigh unto all them that call upon him, to all that call upon him in truth.'
			},
			{
				v: 'Psalm 145:19-21',
				t: 'He will fulfil the desire of them that fear him: he also will hear their cry, and will save them. The <span class="small-caps">Lord</span> preserveth all them that love him: but all the wicked will he destroy. My mouth shall speak the praise of the <span class="small-caps">Lord</span>: and let all flesh bless his holy name for ever and ever.'
			},
			// Proverbs
			{
				v: 'Proverbs 1:5',
				t: 'A wise <em>man</em> will hear, and will increase learning; and a man of understanding shall attain unto wise counsels:'
			},
			{
				v: 'Proverbs 1:7',
				t: 'The fear of the <span class="small-caps">Lord</span> <em>is</em> the beginning of knowledge: <em>but</em> fools despise wisdom and instruction.'
			},
			{
				v: 'Proverbs 1:29-31',
				t: 'For that they hated knowledge, and did not choose the fear of the <span class="small-caps">Lord</span>: They would none of my counsel: they despised all my reproof. Therefore shall they eat of the fruit of their own way, and be filled with their own devices.'
			},
			{
				v: 'Proverbs 1:33',
				t: 'But whoso hearkeneth unto me shall dwell safely, and shall be quiet from fear of evil.'
			},
			{
				v: 'Proverbs 3:5-6',
				t: 'Trust in the <span class="small-caps">Lord</span> with all thine heart; and lean not unto thine own understanding. In all thy ways acknowledge him, and he shall direct thy paths.'
			},
			{
				v: 'Proverbs 3:7',
				t: 'Be not wise in thine own eyes: fear the <span class="small-caps">Lord</span>, and depart from evil.'
			},
			{
				v: 'Proverbs 3:9-10',
				t: 'Honour the <span class="small-caps">Lord</span> with thy substance, and with the firstfruits of all thine increase: So shall thy barns be filled with plenty, and thy presses shall burst out with new wine.'
			},
			{
				v: 'Proverbs 6:16-19',
				t: 'These six <em>things</em> doth the <span class="small-caps">Lord</span> hate: yea, seven <em>are</em> an abomination unto him: A proud look, a lying tongue, and hands that shed innocent blood, An heart that deviseth wicked imaginations, feet that be swift in running to mischief, A false witness <em>that</em> speaketh lies, and he that soweth discord among brethren.'
			},
			{
				v: 'Proverbs 7:2-3',
				t: 'Keep my commandments, and live; and my law as the apple of thine eye. Bind them upon thy fingers, write them upon the table of thine heart.'
			},
			{
				v: 'Proverbs 8:13',
				t: 'The fear of the <span class="small-caps">Lord</span> <em>is</em> to hate evil: pride, and arrogancy, and the evil way, and the froward mouth, do I hate.'
			},
			{
				v: 'Proverbs 9:10',
				t: 'The fear of the Lord <em>is</em> the beginning of wisdom: and the knowledge of the holy <em>is</em> understanding.'
			},
			{
				v: 'Proverbs 10:2',
				t: 'Treasures of wickedness profit nothing: but righteousness delivereth from death.'
			},
			{
				v: 'Proverbs 10:27-29',
				t: 'The fear of the <span class="small-caps">Lord</span> prolongeth days: but the years of the wicked shall be shortened. The hope of the righteous <em>shall be</em> gladness: but the expectation of the wicked shall perish. The way of the <span class="small-caps">Lord</span> <em>is</em> strength to the upright: but destruction <em>shall be</em> to the workers of iniquity.'
			},
			{
				v: 'Proverbs 11:4',
				t: 'Riches profit not in the day of wrath: but righteousness delivereth from death.'
			},
			{
				v: 'Proverbs 12:11',
				t: 'He that tilleth his land shall be satisfied with bread: but he that followeth vain <em>persons is</em> void of understanding.'
			},
			{
				v: 'Proverbs 12:18',
				t: 'There is that speaketh like the piercings of a sword: but the tongue of the wise <em>is</em> health.'
			},
			{
				v: 'Proverbs 12:22',
				t: 'Lying lips <em>are</em> abomination to the <span class="small-caps">Lord</span>: but they that deal truly <em>are</em> his delight.'
			},
			{
				v: 'Proverbs 13:13',
				t: 'Whoso despiseth the word shall be destroyed: but he that feareth the commandment shall be rewarded.'
			},
			{
				v: 'Proverbs 13:20',
				t: 'He that walketh with wise <em>men</em> shall be wise: but a companion of fools shall be destroyed.'
			},
			{
				v: 'Proverbs 14:12',
				t: 'There is a way which seemeth right unto a man, but the end thereof <em>are</em> the ways of death.'
			},
			{
				v: 'Proverbs 14:29',
				t: '<em>He that is</em> slow to wrath <em>is</em> of great understanding: but <em>he that is</em> hasty of spirit exalteth folly.'
			},
			{
				v: 'Proverbs 15:1',
				t: 'A soft answer turneth away wrath: but grievous words stir up anger.'
			},
			{
				v: 'Proverbs 15:3',
				t: 'The eyes of the <span class="small-caps">Lord</span> <em>are</em> in every place, beholding the evil and the good.'
			},
			{
				v: 'Proverbs 16:3',
				t: 'Commit thy works unto the <span class="small-caps">Lord</span>, and thy thoughts shall be established.'
			},
			{
				v: 'Proverbs 16:18',
				t: 'Pride <em>goeth</em> before destruction, and an haughty spirit before a fall.'
			},
			{
				v: 'Proverbs 16:32',
				t: '<em>He that is</em> slow to anger <em>is</em> better than the mighty; and he that ruleth his spirit than he that taketh a city.'
			},
			{
				v: 'Proverbs 17:15',
				t: 'He that justifieth the wicked, and he that condemneth the just, even they both <em>are</em> abomination to the <span class="small-caps">Lord</span>.'
			},
			{
				v: 'Proverbs 17:22',
				t: 'A merry heart doeth good <em>like</em> a medicine: but a broken spirit drieth the bones.'
			},
			{
				v: 'Proverbs 21:20',
				t: '<em>There is</em> treasure to be desired and oil in the dwelling of the wise; but a foolish man spendeth it up.'
			},
			{
				v: 'Proverbs 21:23',
				t: 'Whoso keepeth his mouth and his tongue keepeth his soul from troubles.'
			},
			{
				v: 'Proverbs 22:2',
				t: 'The rich and poor meet together: the <span class="small-caps">Lord</span> <em>is</em> the maker of them all.'
			},
			{
				v: 'Proverbs 22:6',
				t: 'Train up a child in the way he should go: and when he is old, he will not depart from it.'
			},
			{
				v: 'Proverbs 22:7',
				t: 'The rich ruleth over the poor, and the borrower <em>is</em> servant to the lender.'
			},
			{
				v: 'Proverbs 22:15',
				t: 'Foolishness <em>is</em> bound in the heart of a child; <em>but</em> the rod of correction shall drive it far from him.'
			},
			{
				v: 'Proverbs 24:19-20',
				t: 'Fret not thyself because of evil <em>men</em>, neither be thou envious at the wicked; For there shall be no reward to the evil <em>man</em>; the candle of the wicked shall be put out.'
			},
			{
				v: 'Proverbs 27:1',
				t: 'Boast not thyself of to morrow; for thou knowest not what a day may bring forth.'
			},
			{
				v: 'Proverbs 27:17',
				t: 'Iron sharpeneth iron; so a man sharpeneth the countenance of his friend.'
			},
			{
				v: 'Proverbs 27:19',
				t: 'As in water face <em>answereth</em> to face, so the heart of man to man.'
			},
			{
				v: 'Proverbs 27:21',
				t: '<em>As</em> the fining pot for silver, and the furnace for gold; so <em>is</em> a man to his praise.'
			},
			{
				v: 'Proverbs 28:1',
				t: 'The wicked flee when no man pursueth: but the righteous are bold as a lion.'
			},
			{
				v: 'Proverbs 28:13',
				t: 'He that covereth his sins shall not prosper: but whoso confesseth and forsaketh <em>them</em> shall have mercy.'
			},
			{
				v: 'Proverbs 29:4',
				t: 'The king by judgment establisheth the land: but he that receiveth gifts overthroweth it.'
			},
			{
				v: 'Proverbs 29:11',
				t: 'A fool uttereth all his mind: but a wise <em>man</em> keepeth it in till afterwards.'
			},
			{
				v: 'Proverbs 30:5',
				t: 'Every word of God <em>is</em> pure: he <em>is</em> a shield unto them that put their trust in him.'
			},
			{
				v: 'Proverbs 31:8-9',
				t: 'Open thy mouth for the dumb in the cause of all such as are appointed to destruction. Open thy mouth, judge righteously, and plead the cause of the poor and needy.'
			},
			{
				v: 'Proverbs 31:10',
				t: 'Who can find a virtuous woman? for her price <em>is</em> far above rubies.'
			},
			{
				v: 'Proverbs 31:30',
				t: 'Favour <em>is</em> deceitful, and beauty <em>is</em> vain: <em>but</em> a woman <em>that</em> feareth the <span class="small-caps">Lord</span>, she shall be praised.'
			},
			// Ecclesiastes
			{
				v: 'Ecclesiastes 1:9',
				t: 'The thing that hath been, it <em>is that</em> which shall be; and that which is done <em>is</em> that which shall be done: and <em>there is</em> no new <em>thing</em> under the sun.'
			},
			{
				v: 'Ecclesiastes 7:20',
				t: 'For <em>there is</em> not a just man upon earth, that doeth good, and sinneth not.'
			},
			// Isaiah
			{
				v: 'Isaiah 5:20',
				t: 'Woe unto them that call evil good, and good evil; that put darkness for light, and light for darkness; that put bitter for sweet, and sweet for bitter!'
			},
			{
				v: 'Isaiah 6:3',
				t: 'And one cried unto another, and said, Holy, holy, holy, <em>is</em> the <span class="small-caps">Lord</span> of hosts: the whole earth <em>is</em> full of his glory.'
			},
			{
				v: 'Isaiah 7:14',
				t: 'Therefore the Lord himself shall give you a sign; Behold, a virgin shall conceive, and bear a son, and shall call his name Immanuel.'
			},
			{
				v: 'Isaiah 8:20',
				t: 'To the law and to the testimony: if they speak not according to this word, <em>it is</em> because <em>there is</em> no light in them.'
			},
			{
				v: 'Isaiah 9:6',
				t: 'For unto us a child is born, unto us a son is given: and the government shall be upon his shoulder: and his name shall be called Wonderful, Counsellor, The mighty God, The everlasting Father, The Prince of Peace.'
			},
			{
				v: 'Isaiah 14:27',
				t: 'For the <span class="small-caps">Lord</span> of hosts hath purposed, and who shall disannul <em>it</em>? and his hand <em>is</em> stretched out, and who shall turn it back?'
			},
			{
				v: 'Isaiah 33:5',
				t: 'The <span class="small-caps">Lord</span> is exalted; for he dwelleth on high: he hath filled Zion with judgment and righteousness.'
			},
			{
				v: 'Isaiah 40:31',
				t: 'But they that wait upon the <span class="small-caps">Lord</span> shall renew <em>their</em> strength; they shall mount up with wings as eagles; they shall run, and not be weary; <em>and</em> they shall walk, and not faint.'
			},
			{
				v: 'Isaiah 42:8',
				t: 'I <em>am</em> the <span class="small-caps">Lord</span>: that <em>is</em> my name: and my glory will I not give to another, neither my praise to graven images.'
			},
			{
				v: 'Isaiah 43:11',
				t: 'I, <em>even</em> I, <em>am</em> the <span class="small-caps">Lord</span>; and beside me <em>there is</em> no saviour.'
			},
			{
				v: 'Isaiah 43:15',
				t: 'I <em>am</em> the <span class="small-caps">Lord</span>, your Holy One, the creator of Israel, your King.'
			},
			{
				v: 'Isaiah 46:9',
				t: 'Remember the former things of old: for I <em>am</em> God, and <em>there is</em> none else; <em>I am</em> God, and <em>there is</em> none like me,'
			},
			{
				v: 'Isaiah 48:22',
				t: '<em>There is</em> no peace, saith the <span class="small-caps">Lord</span>, unto the wicked.'
			},
			{
				v: 'Isaiah 49:6',
				t: 'And he said, It is a light thing that thou shouldest be my servant to raise up the tribes of Jacob, and to restore the preserved of Israel: I will also give thee for a light to the Gentiles, that thou mayest be my salvation unto the end of the earth.'
			},
			{
				v: 'Isaiah 53:3',
				t: 'He is despised and rejected of men; a man of sorrows, and acquainted with grief: and we hid as it were <em>our</em> faces from him; he was despised, and we esteemed him not.'
			},
			{
				v: 'Isaiah 53:4',
				t: 'Surely he hath borne our griefs, and carried our sorrows: yet we did esteem him stricken, smitten of God, and afflicted.'
			},
			{
				v: 'Isaiah 53:5',
				t: 'But he <em>was</em> wounded for our transgressions, <em>he was</em> bruised for our iniquities: the chastisement of our peace <em>was</em> upon him; and with his stripes we are healed.'
			},
			{
				v: 'Isaiah 53:6',
				t: 'All we like sheep have gone astray; we have turned every one to his own way; and the Lord hath laid on him the iniquity of us all.'
			},
			{
				v: 'Isaiah 53:10',
				t: 'Yet it pleased the <span class="small-caps">Lord</span> to bruise him; he hath put <em>him</em> to grief: when thou shalt make his soul an offering for sin, he shall see <em>his</em> seed, he shall prolong <em>his</em> days, and the pleasure of the <span class="small-caps">Lord</span> shall prosper in his hand.'
			},
			{
				v: 'Isaiah 55:7',
				t: 'Let the wicked forsake his way, and the unrighteous man his thoughts: and let him return unto the <span class="small-caps">Lord</span>, and he will have mercy upon him; and to our God, for he will abundantly pardon.'
			},
			{
				v: 'Isaiah 55:8-9',
				t: 'For my thoughts <em>are</em> not your thoughts, neither <em>are</em> your ways my ways, saith the <span class="small-caps">Lord</span>. For <em>as</em> the heavens are higher than the earth, so are my ways higher than your ways, and my thoughts than your thoughts.'
			},
			{
				v: 'Isaiah 57:21',
				t: '<em>There is</em> no peace, saith my God, to the wicked.'
			},
			{
				v: 'Isaiah 66:2',
				t: 'For all those <em>things</em> hath mine hand made, and all those <em>things</em> have been, saith the <span class="small-caps">Lord</span>: but to this <em>man</em> will I look, <em>even</em> to <em>him that is</em> poor and of a contrite spirit, and trembleth at my word.'
			},
			// Jeremiah
			{
				v: 'Jeremiah 2:19',
				t: 'Thine own wickedness shall correct thee, and thy backslidings shall reprove thee: know therefore and see that <em>it is</em> an evil <em>thing</em> and bitter, that thou hast forsaken the <span class="small-caps">Lord</span> thy God, and that my fear <em>is</em> not in thee, saith the Lord <span class="small-caps">God</span> of hosts.'
			},
			{
				v: 'Jeremiah 9:24',
				t: 'But let him that glorieth glory in this, that he understandeth and knoweth me, that I <em>am</em> the <span class="small-caps">Lord</span> which exercise lovingkindness, judgment, and righteousness, in the earth: for in these <em>things</em> I delight, saith the <span class="small-caps">Lord</span>.'
			},
			{
				v: 'Jeremiah 17:5',
				t: 'Thus saith the <span class="small-caps">Lord</span>; Cursed <em>be</em> the man that trusteth in man, and maketh flesh his arm, and whose heart departeth from the <span class="small-caps">Lord</span>.'
			},
			{
				v: 'Jeremiah 17:7',
				t: 'Blessed <em>is</em> the man that trusteth in the <span class="small-caps">Lord</span>, and whose hope the <span class="small-caps">Lord</span> is.'
			},
			{
				v: 'Jeremiah 17:9',
				t: 'The heart <em>is</em> deceitful above all <em>things</em>, and desperately wicked: who can know it?'
			},
			{
				v: 'Jeremiah 17:10',
				t: 'I the <span class="small-caps">Lord</span> search the heart, <em>I</em> try the reins, even to give every man according to his ways, <em>and</em> according to the fruit of his doings.'
			},
			{
				v: 'Jeremiah 17:14',
				t: 'Heal me, O <span class="small-caps">Lord</span>, and I shall be healed; save me, and I shall be saved: for thou <em>art</em> my praise.'
			},
			{
				v: 'Jeremiah 20:13',
				t: 'Sing unto the <span class="small-caps">Lord</span>, praise ye the <span class="small-caps">Lord</span>: for he hath delivered the soul of the poor from the hand of evildoers.'
			},
			{
				v: 'Jeremiah 23:23-24',
				t: '<em>Am</em> I a God at hand, saith the <span class="small-caps">Lord</span>, and not a God afar off? Can any hide himself in secret places that I shall not see him? saith the <span class="small-caps">Lord</span>. Do not I fill heaven and earth? saith the <span class="small-caps">Lord</span>.'
			},
			{
				v: 'Jeremiah 29:11',
				t: 'For I know the thoughts that I think toward you, saith the <span class="small-caps">Lord</span>, thoughts of peace, and not of evil, to give you an expected end.'
			},
			{
				v: 'Jeremiah 29:13',
				t: 'And ye shall seek me, and find <em>me</em>, when ye shall search for me with all your heart.'
			},
			{
				v: 'Jeremiah 31:25',
				t: 'For I have satiated the weary soul, and I have replenished every sorrowful soul.'
			},
			{
				v: 'Jeremiah 32:27',
				t: 'Behold, I <em>am</em> the <span class="small-caps">Lord</span>, the God of all flesh: is there any thing too hard for me?'
			},
			// Lamentations
			{
				v: 'Lamentations 5:19',
				t: 'Thou, O <span class="small-caps">Lord</span>, remainest for ever; thy throne from generation to generation.'
			},
			// Ezekiel
			{
				v: 'Ezekiel 33:11',
				t: 'Say unto them, <em>As</em> I live, saith the Lord <span class="small-caps">GOD</span>, I have no pleasure in the death of the wicked; but that the wicked turn from his way and live: turn ye, turn ye from your evil ways; for why will ye die, O house of Israel?'
			},
			// Hosea
			{
				v: 'Hosea 4:6',
				t: 'My people are destroyed for lack of knowledge: because thou hast rejected knowledge, I will also reject thee, that thou shalt be no priest to me: seeing thou hast forgotten the law of thy God, I will also forget thy children.'
			},
			{
				v: 'Hosea 13:4',
				t: 'Yet I <em>am</em> the LORD thy God from the land of Egypt, and thou shalt know no god but me: for <em>there is</em> no saviour beside me.'
			},
			// Habakkuk
			{
				v: 'Habakkuk 2:4',
				t: 'Behold, his soul <em>which</em> is lifted up is not upright in him: but the just shall live by his faith'
			},
			{
				v: 'Habakkuk 3:17-18',
				t: 'Although the fig tree shall not blossom, neither <em>shall</em> fruit <em>be</em> in the vines; the labour of the olive shall fail, and the fields shall yield no meat; the flock shall be cut off from the fold, and <em>there shall be</em> no herd in the stalls: Yet I will rejoice in the <span class="small-caps">Lord</span>, I will joy in the God of my salvation.'
			},
			// Matthew
			{
				v: 'Matthew 1:21',
				t: 'And she shall bring forth a son, and thou shalt call his name <span class="small-caps">Jesus</span>: for he shall save his people from their sins.'
			},
			{
				v: 'Matthew 3:1-2',
				t: 'In those days came John the Baptist, preaching in the wilderness of Judaea, And saying, Repent ye: for the kingdom of heaven is at hand.'
			},
			{
				v: 'Matthew 4:4',
				t: 'But he answered and said, <span class="red">It is written, Man shall not live by bread alone, but by every word that proceedeth out of the mouth of God.</span>'
			},
			{
				v: 'Matthew 5:3',
				t: '<span class="red">Blessed <em>are</em> the poor in spirit: for theirs is the kingdom of heaven.</span>'
			},
			{
				v: 'Matthew 5:4',
				t: '<span class="red">Blessed <em>are</em> they that mourn: for they shall be comforted.</span>'
			},
			{
				v: 'Matthew 5:5',
				t: '<span class="red">Blessed <em>are</em> the meek: for they shall inherit the earth.</span>'
			},
			{
				v: 'Matthew 5:6',
				t: '<span class="red">Blessed <em>are</em> they which do hunger and thirst after righteousness: for they shall be filled.</span>'
			},
			{
				v: 'Matthew 5:7',
				t: '<span class="red">Blessed <em>are</em> the merciful: for they shall obtain mercy.</span>'
			},
			{
				v: 'Matthew 5:8',
				t: '<span class="red">Blessed <em>are</em> the pure in heart: for they shall see God.</span>'
			},
			{
				v: 'Matthew 5:9',
				t: '<span class="red">Blessed <em>are</em> the peacemakers: for they shall be called the children of God.</span>'
			},
			{
				v: 'Matthew 5:10',
				t: '<span class="red">Blessed <em>are</em> they which are persecuted for righteousness\' sake: for theirs is the kingdom of heaven.</span>'
			},
			{
				v: 'Matthew 5:11-12',
				t: '<span class="red">Blessed are ye, when <em>men</em> shall revile you, and persecute <em>you</em>, and shall say all manner of evil against you falsely, for my sake. Rejoice, and be exceeding glad: for great <em>is</em> your reward in heaven: for so persecuted they the prophets which were before you.</span>'
			},
			{
				v: 'Matthew 5:13',
				t: '<span class="red">Ye are the salt of the earth: but if the salt have lost his savour, wherewith shall it be salted? it is thenceforth good for nothing, but to be cast out, and to be trodden under foot of men.</span>'
			},
			{
				v: 'Matthew 5:14',
				t: '<span class="red">Ye are the light of the world. A city that is set on an hill cannot be hid.</span>'
			},
			{
				v: 'Matthew 5:16',
				t: '<span class="red">Let your light so shine before men, that they may see your good works, and glorify your Father which is in heaven.</span>'
			},
			{
				v: 'Matthew 5:17',
				t: '<span class="red">Think not that I am come to destroy the law, or the prophets: I am not come to destroy, but to fulfil.</span>'
			},
			{
				v: 'Matthew 5:18',
				t: '<span class="red">For verily I say unto you, Till heaven and earth pass, one jot or one tittle shall in no wise pass from the law, till all be fulfilled.</span>'
			},
			{
				v: 'Matthew 5:19',
				t: '<span class="red">Whosoever therefore shall break one of these least commandments, and shall teach men so, he shall be called the least in the kingdom of heaven: but whosoever shall do and teach <em>them</em>, the same shall be called great in the kingdom of heaven.</span>'
			},
			{
				v: 'Matthew 5:22',
				t: '<span class="red">But I say unto you, That whosoever is angry with his brother without a cause shall be in danger of the judgment: and whosoever shall say to his brother, Raca, shall be in danger of the council: but whosoever shall say, Thou fool, shall be in danger of hell fire.</span>'
			},
			{
				v: 'Matthew 5:25',
				t: '<span class="red">Agree with thine adversary quickly, whiles thou art in the way with him; lest at any time the adversary deliver thee to the judge, and the judge deliver thee to the officer, and thou be cast into prison.</span>'
			},
			{
				v: 'Matthew 5:27-28',
				t: '<span class="red">Ye have heard that it was said by them of old time, Thou shalt not commit adultery: But I say unto you, That whosoever looketh on a woman to lust after her hath committed adultery with her already in his heart.</span>'
			},
			{
				v: 'Matthew 5:29',
				t: '<span class="red">And if thy right eye offend thee, pluck it out, and cast <em>it</em> from thee: for it is profitable for thee that one of thy members should perish, and not <em>that</em> thy whole body should be cast into hell.</span>'
			},
			{
				v: 'Matthew 5:30',
				t: '<span class="red">And if thy right hand offend thee, cut it off, and cast <em>it</em> from thee: for it is profitable for thee that one of thy members should perish, and not <em>that</em> thy whole body should be cast into hell.</span>'
			},
			{
				v: 'Matthew 5:36-37',
				t: '<span class="red">Neither shalt thou swear by thy head, because thou canst not make one hair white or black. But let your communication be, Yea, yea; Nay, nay: for whatsoever is more than these cometh of evil.</span>'
			},
			{
				v: 'Matthew 5:38-39',
				t: '<span class="red">Ye have heard that it hath been said, An eye for an eye, and a tooth for a tooth: But I say unto you, That ye resist not evil: but whosoever shall smite thee on thy right cheek, turn to him the other also.</span>'
			},
			{
				v: 'Matthew 5:40-41',
				t: '<span class="red">And if any man will sue thee at the law, and take away thy coat, let him have <em>thy</em> cloak also. And whosoever shall compel thee to go a mile, go with him twain.</span>'
			},
			{
				v: 'Matthew 5:44-45',
				t: '<span class="red">But I say unto you, Love your enemies, bless them that curse you, do good to them that hate you, and pray for them which despitefully use you, and persecute you; That ye may be the children of your Father which is in heaven: for he maketh his sun to rise on the evil and on the good, and sendeth rain on the just and on the unjust.</span>'
			},
			{
				v: 'Matthew 5:48',
				t: '<span class="red">Be ye therefore perfect, even as your Father which is in heaven is perfect.</span>'
			},
			{
				v: 'Matthew 6:1',
				t: '<span class="red">Take heed that ye do not your alms before men, to be seen of them: otherwise ye have no reward of your Father which is in heaven.</span>'
			},
			{
				v: 'Matthew 6:3-4',
				t: '<span class="red">But when thou doest alms, let not thy left hand know what thy right hand doeth: That thine alms may be in secret: and thy Father which seeth in secret himself shall reward thee openly.</span>'
			},
			{
				v: 'Matthew 6:5',
				t: '<span class="red">And when thou prayest, thou shalt not be as the hypocrites <em>are</em>: for they love to pray standing in the synagogues and in the corners of the streets, that they may be seen of men. Verily I say unto you, They have their reward.</span>'
			},
			{
				v: 'Matthew 6:6',
				t: '<span class="red">But thou, when thou prayest, enter into thy closet, and when thou hast shut thy door, pray to thy Father which is in secret; and thy Father which seeth in secret shall reward thee openly.</span>'
			},
			{
				v: 'Matthew 6:7',
				t: '<span class="red">But when ye pray, use not vain repetitions, as the heathen <em>do</em>: for they think that they shall be heard for their much speaking.</span>'
			},
			{
				v: 'Matthew 6:17-18',
				t: '<span class="red">But thou, when thou fastest, anoint thine head, and wash thy face; That thou appear not unto men to fast, but unto thy Father which is in secret: and thy Father, which seeth in secret, shall reward thee openly.</span>'
			},
			{
				v: 'Matthew 6:19-21',
				t: '<span class="red">Lay not up for yourselves treasures upon earth, where moth and rust doth corrupt, and where thieves break through and steal: But lay up for yourselves treasures in heaven, where neither moth nor rust doth corrupt, and where thieves do not break through nor steal: For where your treasure is, there will your heart be also.</span>'
			},
			{
				v: 'Matthew 6:22-23',
				t: '<span class="red">The light of the body is the eye: if therefore thine eye be single, thy whole body shall be full of light. But if thine eye be evil, thy whole body shall be full of darkness. If therefore the light that is in thee be darkness, how great <em>is</em> that darkness!</span>'
			},
			{
				v: 'Matthew 6:24',
				t: '<span class="red">No man can serve two masters: for either he will hate the one, and love the other; or else he will hold to the one, and despise the other. Ye cannot serve God and mammon.</span>'
			},
			{
				v: 'Matthew 6:26',
				t: '<span class="red">Behold the fowls of the air: for they sow not, neither do they reap, nor gather into barns; yet your heavenly Father feedeth them. Are ye not much better than they?</span>'
			},
			{
				v: 'Matthew 6:33',
				t: '<span class="red">But seek ye first the kingdom of God, and his righteousness; and all these things shall be added unto you.</span>'
			},
			{
				v: 'Matthew 6:34',
				t: '<span class="red">Take therefore no thought for the morrow: for the morrow shall take thought for the things of itself. Sufficient unto the day <em>is</em> the evil thereof.</span>'
			},
			{
				v: 'Matthew 7:1-2',
				t: '<span class="red">Judge not, that ye be not judged. For with what judgment ye judge, ye shall be judged: and with what measure ye mete, it shall be measured to you again.</span>'
			},
			{
				v: 'Matthew 7:6',
				t: '<span class="red">Give not that which is holy unto the dogs, neither cast ye your pearls before swine, lest they trample them under their feet, and turn again and rend you.</span>'
			},
			{
				v: 'Matthew 7:7-8',
				t: '<span class="red">Ask, and it shall be given you; seek, and ye shall find; knock, and it shall be opened unto you: For every one that asketh receiveth; and he that seeketh findeth; and to him that knocketh it shall be opened.</span>'
			},
			{
				v: 'Matthew 7:9-11',
				t: '<span class="red">Or what man is there of you, whom if his son ask bread, will he give him a stone? Or if he ask a fish, will he give him a serpent? If ye then, being evil, know how to give good gifts unto your children, how much more shall your Father which is in heaven give good things to them that ask him?</span>'
			},
			{
				v: 'Matthew 7:12',
				t: '<span class="red">Therefore all things whatsoever ye would that men should do to you, do ye even so to them: for this is the law and the prophets.</span>'
			},
			{
				v: 'Matthew 7:13-14',
				t: '<span class="red">Enter ye in at the strait gate: for wide <em>is</em> the gate, and broad <em>is</em> the way, that leadeth to destruction, and many there be which go in thereat: Because strait <em>is</em> the gate, and narrow <em>is</em> the way, which leadeth unto life, and few there be that find it.</span>'
			},
			{
				v: 'Matthew 7:15-16',
				t: '<span class="red">Beware of false prophets, which come to you in sheep\'s clothing, but inwardly they are ravening wolves. Ye shall know them by their fruits. Do men gather grapes of thorns, or figs of thistles?</span>'
			},
			{
				v: 'Matthew 7:18-20',
				t: '<span class="red">A good tree cannot bring forth evil fruit, neither <em>can</em> a corrupt tree bring forth good fruit. Every tree that bringeth not forth good fruit is hewn down, and cast into the fire. Wherefore by their fruits ye shall know them.</span>'
			},
			{
				v: 'Matthew 7:21',
				t: '<span class="red">Not every one that saith unto me, Lord, Lord, shall enter into the kingdom of heaven; but he that doeth the will of my Father which is in heaven.</span>'
			},
			{
				v: 'Matthew 7:22-23',
				t: '<span class="red">Many will say to me in that day, Lord, Lord, have we not prophesied in thy name? and in thy name have cast out devils? and in thy name done many wonderful works? And then will I profess unto them, I never knew you: depart from me, ye that work iniquity.</span>'
			},
			{
				v: 'Matthew 7:24-25',
				t: '<span class="red">Therefore whosoever heareth these sayings of mine, and doeth them, I will liken him unto a wise man, which built his house upon a rock: And the rain descended, and the floods came, and the winds blew, and beat upon that house; and it fell not: for it was founded upon a rock.</span>'
			},
			{
				v: 'Matthew 9:37',
				t: 'Then saith he unto his disciples, <span class="red">The harvest truly <em>is</em> plenteous, but the labourers <em>are</em> few;</span>'
			},
			{
				v: 'Matthew 10:14-15',
				t: '<span class="red">And whosoever shall not receive you, nor hear your words, when ye depart out of that house or city, shake off the dust of your feet. Verily I say unto you, It shall be more tolerable for the land of Sodom and Gomorrha in the day of judgment, than for that city.</span>'
			},
			{
				v: 'Matthew 10:16',
				t: '<span class="red">Behold, I send you forth as sheep in the midst of wolves: be ye therefore wise as serpents, and harmless as doves.</span>'
			},
			{
				v: 'Matthew 10:28',
				t: '<span class="red">And fear not them which kill the body, but are not able to kill the soul: but rather fear him which is able to destroy both soul and body in hell.</span>'
			},
			{
				v: 'Matthew 10:32',
				t: '<span class="red">Whosoever therefore shall confess me before men, him will I confess also before my Father which is in heaven.</span>'
			},
			{
				v: 'Matthew 10:34',
				t: '<span class="red">Think not that I am come to send peace on earth: I came not to send peace, but a sword.</span>'
			},
			{
				v: 'Matthew 10:35-36',
				t: '<span class="red">For I am come to set a man at variance against his father, and the daughter against her mother, and the daughter in law against her mother in law. And a man\'s foes <em>shall be</em> they of his own household.</span>'
			},
			{
				v: 'Matthew 10:37-38',
				t: '<span class="red">He that loveth father or mother more than me is not worthy of me: and he that loveth son or daughter more than me is not worthy of me. And he that taketh not his cross, and followeth after me, is not worthy of me.</span>'
			},
			{
				v: 'Matthew 10:39',
				t: '<span class="red">He that findeth his life shall lose it: and he that loseth his life for my sake shall find it.</span>'
			},
			{
				v: 'Matthew 11:28',
				t: '<span class="red">Come unto me, all <em>ye</em> that labour and are heavy laden, and I will give you rest.</span>'
			},
			{
				v: 'Matthew 11:29-30',
				t: '<span class="red">Take my yoke upon you, and learn of me; for I am meek and lowly in heart: and ye shall find rest unto your souls. For my yoke <em>is</em> easy, and my burden is light.</span>'
			},
			{
				v: 'Matthew 12:30',
				t: '<span class="red">He that is not with me is against me; and he that gathereth not with me scattereth abroad.</span>'
			},
			{
				v: 'Matthew 13:45-46',
				t: '<span class="red">Again, the kingdom of heaven is like unto a merchant man, seeking goodly pearls: Who, when he had found one pearl of great price, went and sold all that he had, and bought it.</span>'
			},
			{
				v: 'Matthew 15:9',
				t: '<span class="red">But in vain they do worship me, teaching <em>for</em> doctrines the commandments of men.</span>'
			},
			{
				v: 'Matthew 16:26',
				t: '<span class="red">For what is a man profited, if he shall gain the whole world, and lose his own soul? or what shall a man give in exchange for his soul?</span>'
			},
			{
				v: 'Matthew 16:24-25',
				t: 'Then said Jesus unto his disciples, <span class="red">If any <em>man</em> will come after me, let him deny himself, and take up his cross, and follow me. For whosoever will save his life shall lose it: and whosoever will lose his life for my sake shall find it.</span>'
			},
			{
				v: 'Matthew 16:27',
				t: '<span class="red">For the Son of man shall come in the glory of his Father with his angels; and then he shall reward every man according to his works.</span>'
			},
			{
				v: 'Matthew 17:20',
				t: 'And Jesus said unto them, <span class="red">Because of your unbelief: for verily I say unto you, If ye have faith as a grain of mustard seed, ye shall say unto this mountain, Remove hence to yonder place; and it shall remove; and nothing shall be impossible unto you.</span>'
			},
			{
				v: 'Matthew 18:3',
				t: 'And said, <span class="red">Verily I say unto you, Except ye be converted, and become as little children, ye shall not enter into the kingdom of heaven.</span>'
			},
			{
				v: 'Matthew 18:11',
				t: '<span class="red">For the Son of man is come to save that which was lost.</span>'
			},
			{
				v: 'Matthew 18:20',
				t: '<span class="red">For where two or three are gathered together in my name, there am I in the midst of them.</span>'
			},
			{
				v: 'Matthew 19:26',
				t: 'But Jesus beheld <em>them</em>, and said unto them, <span class="red">With men this is impossible; but with God all things are possible.</span>'
			},
			{
				v: 'Matthew 20:16',
				t: '<span class="red">So the last shall be first, and the first last: for many be called, but few chosen.</span>'
			},
			{
				v: 'Matthew 20:28',
				t: '<span class="red">Even as the Son of man came not to be ministered unto, but to minister, and to give his life a ransom for many.</span>'
			},
			{
				v: 'Matthew 22:14',
				t: '<span class="red">For many are called, but few <em>are</em> chosen.</span>'
			},
			{
				v: 'Matthew 22:21',
				t: 'They say unto him, Caesar\'s. Then saith he unto them, <span class="red">Render therefore unto Caesar the things which are Caesar\'s; and unto God the things that are God\'s.</span>'
			},
			{
				v: 'Matthew 22:37-39',
				t: 'Jesus said unto him, <span class="red">Thou shalt love the Lord thy God with all thy heart, and with all thy soul, and with all thy mind. This is the first and great commandment. And the second <em>is</em> like unto it, Thou shalt love thy neighbour as thyself.</span>'
			},
			{
				v: 'Matthew 23:9-11',
				t: '<span class="red">And call no <em>man</em> your father upon the earth: for one is your Father, which is in heaven. Neither be ye called masters: for one is your Master, <em>even</em> Christ. But he that is greatest among you shall be your servant.</span>'
			},
			{
				v: 'Matthew 23:12',
				t: '<span class="red">And whosoever shall exalt himself shall be abased; and he that shall humble himself shall be exalted.</span>'
			},
			{
				v: 'Matthew 23:37',
				t: '<span class="red">O Jerusalem, Jerusalem, <em>thou</em> that killest the prophets, and stonest them which are sent unto thee, how often would I have gathered thy children together, even as a hen gathereth her chickens under <em>her</em> wings, and ye would not!</span>'
			},
			{
				v: 'Matthew 24:5',
				t: '<span class="red">For many shall come in my name, saying, I am Christ; and shall deceive many.</span>'
			},
			{
				v: 'Matthew 24:11',
				t: '<span class="red">And many false prophets shall rise, and shall deceive many.</span>'
			},
			{
				v: 'Matthew 24:13',
				t: '<span class="red">But he that shall endure unto the end, the same shall be saved.</span>'
			},
			{
				v: 'Matthew 24:14',
				t: '<span class="red">And this gospel of the kingdom shall be preached in all the world for a witness unto all nations; and then shall the end come.</span>'
			},
			{
				v: 'Matthew 24:24',
				t: '<span class="red">For there shall arise false Christs, and false prophets, and shall shew great signs and wonders; insomuch that, if <em>it were</em> possible, they shall deceive the very elect.</span>'
			},
			{
				v: 'Matthew 24:30',
				t: '<span class="red">And then shall appear the sign of the Son of man in heaven: and then shall all the tribes of the earth mourn, and they shall see the Son of man coming in the clouds of heaven with power and great glory.</span>'
			},
			{
				v: 'Matthew 24:35-36',
				t: '<span class="red">Heaven and earth shall pass away, but my words shall not pass away. But of that day and hour knoweth no <em>man</em>, no, not the angels of heaven, but my Father only.</span>'
			},
			{
				v: 'Matthew 24:37',
				t: '<span class="red">But as the days of Noe <em>were</em>, so shall also the coming of the Son of man be.</span>'
			},
			{
				v: 'Matthew 24:42',
				t: '<span class="red">Watch therefore: for ye know not what hour your Lord doth come.</span>'
			},
			{
				v: 'Matthew 24:44',
				t: '<span class="red">Therefore be ye also ready: for in such an hour as ye think not the Son of man cometh.</span>'
			},
			{
				v: 'Matthew 25:13',
				t: '<span class="red">Watch therefore, for ye know neither the day nor the hour wherein the Son of man cometh.</span>'
			},
			{
				v: 'Matthew 25:23',
				t: '<span class="red">His lord said unto him, Well done, good and faithful servant; thou hast been faithful over a few things, I will make thee ruler over many things: enter thou into the joy of thy lord.</span>'
			},
			{
				v: 'Matthew 25:29',
				t: '<span class="red">For unto every one that hath shall be given, and he shall have abundance: but from him that hath not shall be taken away even that which he hath.</span>'
			},
			{
				v: 'Matthew 25:31',
				t: '<span class="red">When the Son of man shall come in his glory, and all the holy angels with him, then shall he sit upon the throne of his glory:</span>'
			},
			{
				v: 'Matthew 25:34',
				t: '<span class="red">Then shall the King say unto them on his right hand, Come, ye blessed of my Father, inherit the kingdom prepared for you from the foundation of the world:</span>'
			},
			{
				v: 'Matthew 25:35-36',
				t: '<span class="red">For I was an hungred, and ye gave me meat: I was thirsty, and ye gave me drink: I was a stranger, and ye took me in: Naked, and ye clothed me: I was sick, and ye visited me: I was in prison, and ye came unto me.</span>'
			},
			{
				v: 'Matthew 26:28',
				t: '<span class="red">For this is my blood of the new testament, which is shed for many for the remission of sins.</span>'
			},
			{
				v: 'Matthew 26:41',
				t: '<span class="red">Watch and pray, that ye enter not into temptation: the spirit indeed <em>is</em> willing, but the flesh <em>is</em> weak.</span>'
			},
			{
				v: 'Matthew 28:18',
				t: 'And Jesus came and spake unto them, saying, <span class="red">All power is given unto me in heaven and in earth.</span>'
			},
			{
				v: 'Matthew 28:19',
				t: '<span class="red">Go ye therefore, and teach all nations, baptizing them in the name of the Father, and of the Son, and of the Holy Ghost:</span>'
			},
			// Mark
			{
				v: 'Mark 2:17',
				t: 'When Jesus heard <em>it</em>, he saith unto them, <span class="red">They that are whole have no need of the physician, but they that are sick: I came not to call the righteous, but sinners to repentance.</span>'
			},
			{
				v: 'Mark 4:22',
				t: '<span class="red">For there is nothing hid, which shall not be manifested; neither was any thing kept secret, but that it should come abroad.</span>'
			},
			{
				v: 'Mark 6:11',
				t: '<span class="red">And whosoever shall not receive you, nor hear you, when ye depart thence, shake off the dust under your feet for a testimony against them. Verily I say unto you, It shall be more tolerable for Sodom and Gomorrha in the day of judgment, than for that city.</span>'
			},
			{
				v: 'Mark 7:9',
				t: 'And he said unto them, <span class="red">Full well ye reject the commandment of God, that ye may keep your own tradition.</span>'
			},
			{
				v: 'Mark 7:21-23',
				t: '<span class="red">For from within, out of the heart of men, proceed evil thoughts, adulteries, fornications, murders, Thefts, covetousness, wickedness, deceit, lasciviousness, an evil eye, blasphemy, pride, foolishness: All these evil things come from within, and defile the man.</span>'
			},
			{
				v: 'Mark 8:34',
				t: 'And when he had called the people <em>unto him</em> with his disciples also, he said unto them, <span class="red">Whosoever will come after me, let him deny himself, and take up his cross, and follow me.</span>'
			},
			{
				v: 'Mark 8:35',
				t: '<span class="red">For whosoever will save his life shall lose it; but whosoever shall lose his life for my sake and the gospel\'s, the same shall save it.</span>'
			},
			{
				v: 'Mark 9:43',
				t: '<span class="red">And if thy hand offend thee, cut it off: it is better for thee to enter into life maimed, than having two hands to go into hell, into the fire that never shall be quenched:</span>'
			},
			{
				v: 'Mark 10:18',
				t: 'And Jesus said unto him, <span class="red">Why callest thou me good? <em>there is</em> none good but one, <em>that is</em>, God.</span>'
			},
			{
				v: 'Mark 10:24-25',
				t: 'And the disciples were astonished at his words. But Jesus answereth again, and saith unto them, <span class="red">Children, how hard is it for them that trust in riches to enter into the kingdom of God! It is easier for a camel to go through the eye of a needle, than for a rich man to enter into the kingdom of God.</span>'
			},
			{
				v: 'Mark 10:27',
				t: 'And Jesus looking upon them saith, <span class="red">With men <em>it is</em> impossible, but not with God: for with God all things are possible.</span>'
			},
			{
				v: 'Mark 10:31',
				t: '<span class="red">But many <em>that are</em> first shall be last; and the last first.</span>'
			},
			{
				v: 'Mark 10:45',
				t: '<span class="red">For even the Son of man came not to be ministered unto, but to minister, and to give his life a ransom for many.</span>'
			},
			{
				v: 'Mark 11:24',
				t: '<span class="red">Therefore I say unto you, What things soever ye desire, when ye pray, believe that ye receive <em>them</em>, and ye shall have <em>them</em>.</span>'
			},
			{
				v: 'Mark 11:26',
				t: '<span class="red">But if ye do not forgive, neither will your Father which is in heaven forgive your trespasses.</span>'
			},
			{
				v: 'Mark 12:24',
				t: 'And Jesus answering said unto them, <span class="red">Do ye not therefore err, because ye know not the scriptures, neither the power of God?</span>'
			},
			{
				v: 'Mark 12:30-31',
				t: '<span class="red">And thou shalt love the Lord thy God with all thy heart, and with all thy soul, and with all thy mind, and with all thy strength: this <em>is</em> the first commandment. And the second <em>is</em> like, <em>namely</em> this, Thou shalt love thy neighbour as thyself. There is none other commandment greater than these.</span>'
			},
			{
				v: 'Mark 16:15-16',
				t: 'And he said unto them, <span class="red">Go ye into all the world, and preach the gospel to every creature. He that believeth and is baptized shall be saved; but he that believeth not shall be damned.</span>'
			},
			// Luke
			{
				v: 'Luke 1:49',
				t: 'For he that is mighty hath done to me great things; and holy <em>is</em> his name.'
			},
			{
				v: 'Luke 2:11',
				t: 'For unto you is born this day in the city of David a Saviour, which is Christ the Lord.'
			},
			{
				v: 'Luke 2:14',
				t: 'Glory to God in the highest, and on earth peace, good will toward men.'
			},
			{
				v: 'Luke 6:22',
				t: '<span class="red">Blessed are ye, when men shall hate you, and when they shall separate you <em>from their company</em>, and shall reproach <em>you</em>, and cast out your name as evil, for the Son of man\'s sake.</span>'
			},
			{
				v: 'Luke 6:27-28',
				t: '<span class="red">But I say unto you which hear, Love your enemies, do good to them which hate you, Bless them that curse you, and pray for them which despitefully use you.</span>'
			},
			{
				v: 'Luke 6:30-31',
				t: '<span class="red">Give to every man that asketh of thee; and of him that taketh away thy goods ask <em>them</em> not again. And as ye would that men should do to you, do ye also to them likewise.</span>'
			},
			{
				v: 'Luke 6:46',
				t: '<span class="red">And why call ye me, Lord, Lord, and do not the things which I say?</span>'
			},
			{
				v: 'Luke 8:17',
				t: '<span class="red">For nothing is secret, that shall not be made manifest; neither <em>any thing</em> hid, that shall not be known and come abroad.</span>'
			},
			{
				v: 'Luke 9:23',
				t: 'And he said to <em>them</em> all, <span class="red">If any <em>man</em> will come after me, let him deny himself, and take up his cross daily, and follow me.</span>'
			},
			{
				v: 'Luke 9:49-50',
				t: 'And John answered and said, Master, we saw one casting out devils in thy name; and we forbad him, because he followeth not with us. And Jesus said unto him, <span class="red">Forbid <em>him</em> not: for he that is not against us is for us.</span>'
			},
			{
				v: 'Luke 9:55-56',
				t: 'But he turned, and rebuked them, and said, <span class="red">Ye know not what manner of spirit ye are of. For the Son of man is not come to destroy men\'s lives, but to save <em>them</em>.</span> And they went to another village.'
			},
			{
				v: 'Luke 9:59-60',
				t: 'And he said unto another, <span class="red">Follow me.</span> But he said, Lord, suffer me first to go and bury my father. Jesus said unto him, <span class="red">Let the dead bury their dead: but go thou and preach the kingdom of God.</span>'
			},
			{
				v: 'Luke 9:61-62',
				t: 'And another also said, Lord, I will follow thee; but let me first go bid them farewell, which are at home at my house. And Jesus said unto him, <span class="red">No man, having put his hand to the plough, and looking back, is fit for the kingdom of God.</span>'
			},
			{
				v: 'Luke 10:16',
				t: '<span class="red">He that heareth you heareth me; and he that despiseth you despiseth me; and he that despiseth me despiseth him that sent me.</span>'
			},
			{
				v: 'Luke 10:27',
				t: 'And he answering said, Thou shalt love the Lord thy God with all thy heart, and with all thy soul, and with all thy strength, and with all thy mind; and thy neighbour as thyself.'
			},
			{
				v: 'Luke 11:9',
				t: '<span class="red">And I say unto you, Ask, and it shall be given you; seek, and ye shall find; knock, and it shall be opened unto you.</span>'
			},
			{
				v: 'Luke 11:28',
				t: 'But he said, <span class="red">Yea rather, blessed <em>are</em> they that hear the word of God, and keep it.</span>'
			},
			{
				v: 'Luke 11:33',
				t: '<span class="red">No man, when he hath lighted a candle, putteth <em>it</em> in a secret place, neither under a bushel, but on a candlestick, that they which come in may see the light.</span>'
			},
			{
				v: 'Luke 12:24',
				t: '<span class="red">Consider the ravens: for they neither sow nor reap; which neither have storehouse nor barn; and God feedeth them: how much more are ye better than the fowls?</span>'
			},
			{
				v: 'Luke 12:32',
				t: '<span class="red">Fear not, little flock; for it is your Father\'s good pleasure to give you the kingdom.</span>'
			},
			{
				v: 'Luke 12:40',
				t: '<span class="red">Be ye therefore ready also: for the Son of man cometh at an hour when ye think not.</span>'
			},
			{
				v: 'Luke 13:3',
				t: '<span class="red">I tell you, Nay: but, except ye repent, ye shall all likewise perish.</span>'
			},
			{
				v: 'Luke 13:23-24',
				t: 'Then said one unto him, Lord, are there few that be saved? And he said unto them, <span class="red">Strive to enter in at the strait gate: for many, I say unto you, will seek to enter in, and shall not be able.</span>'
			},
			{
				v: 'Luke 16:13',
				t: '<span class="red">No servant can serve two masters: for either he will hate the one, and love the other; or else he will hold to the one, and despise the other. Ye cannot serve God and mammon.</span>'
			},
			{
				v: 'Luke 16:16-17',
				t: '<span class="red">The law and the prophets <em>were</em> until John: since that time the kingdom of God is preached, and every man presseth into it. And it is easier for heaven and earth to pass, than one tittle of the law to fail.</span>'
			},
			{
				v: 'Luke 18:25',
				t: '<span class="red">For it is easier for a camel to go through a needle\'s eye, than for a rich man to enter into the kingdom of God.</span>'
			},
			{
				v: 'Luke 18:27',
				t: 'And he said, <span class="red">The things which are impossible with men are possible with God.</span>'
			},
			{
				v: 'Luke 19:10',
				t: '<span class="red">For the Son of man is come to seek and to save that which was lost.</span>'
			},
			{
				v: 'Luke 21:8',
				t: 'And he said, <span class="red">Take heed that ye be not deceived: for many shall come in my name, saying, I am <em>Christ</em>; and the time draweth near: go ye not therefore after them.</span>'
			},
			{
				v: 'Luke 22:36',
				t: 'Then said he unto them, <span class="red">But now, he that hath a purse, let him take <em>it</em>, and likewise <em>his</em> scrip: and he that hath no sword, let him sell his garment, and buy one.</span>'
			},
			{
				v: 'Luke 23:34',
				t: 'Then said Jesus, <span class="red">Father, forgive them; for they know not what they do.</span> And they parted his raiment, and cast lots.'
			},
			// John
			{
				v: 'John 1:1',
				t: 'In the beginning was the Word, and the Word was with God, and the Word was God.'
			},
			{
				v: 'John 1:2-3',
				t: 'The same was in the beginning with God. All things were made by him; and without him was not any thing made that was made.'
			},
			{
				v: 'John 1:5',
				t: 'And the light shineth in darkness; and the darkness comprehended it not.'
			},
			{
				v: 'John 1:10',
				t: 'He was in the world, and the world was made by him, and the world knew him not.'
			},
			{
				v: 'John 1:12',
				t: 'But as many as received him, to them gave he power to become the sons of God, <em>even</em> to them that believe on his name:'
			},
			{
				v: 'John 1:14',
				t: 'And the Word was made flesh, and dwelt among us, (and we beheld his glory, the glory as of the only begotten of the Father,) full of grace and truth.'
			},
			{
				v: 'John 1:17',
				t: 'For the law was given by Moses, <em>but</em> grace and truth came by Jesus Christ.'
			},
			{
				v: 'John 1:29',
				t: 'The next day John seeth Jesus coming unto him, and saith, Behold the Lamb of God, which taketh away the sin of the world.'
			},
			{
				v: 'John 2:19',
				t: 'Jesus answered and said unto them, <span class="red">Destroy this temple, and in three days I will raise it up.</span>'
			},
			{
				v: 'John 3:3',
				t: 'Jesus answered and said unto him, <span class="red">Verily, verily, I say unto thee, Except a man be born again, he cannot see the kingdom of God.</span>'
			},
			{
				v: 'John 3:5-6',
				t: 'Jesus answered, <span class="red">Verily, verily, I say unto thee, Except a man be born of water and <em>of</em> the Spirit, he cannot enter into the kingdom of God. That which is born of the flesh is flesh; and that which is born of the Spirit is spirit.</span>'
			},
			{
				v: 'John 3:14-15',
				t: '<span class="red">And as Moses lifted up the serpent in the wilderness, even so must the Son of man be lifted up: That whosoever believeth in him should not perish, but have eternal life.</span>'
			},
			{
				v: 'John 3:16',
				t: '<span class="red">For God so loved the world, that he gave his only begotten Son, that whosoever believeth in him should not perish, but have everlasting life.</span>'
			},
			{
				v: 'John 3:17',
				t: '<span class="red">For God sent not his Son into the world to condemn the world; but that the world through him might be saved.</span>'
			},
			{
				v: 'John 3:18-19',
				t: '<span class="red">He that believeth on him is not condemned: but he that believeth not is condemned already, because he hath not believed in the name of the only begotten Son of God. And this is the condemnation, that light is come into the world, and men loved darkness rather than light, because their deeds were evil.</span>'
			},
			{
				v: 'John 3:20-21',
				t: '<span class="red">For every one that doeth evil hateth the light, neither cometh to the light, lest his deeds should be reproved. But he that doeth truth cometh to the light, that his deeds may be made manifest, that they are wrought in God.</span>'
			},
			{
				v: 'John 3:27',
				t: 'John answered and said, A man can receive nothing, except it be given him from heaven.'
			},
			{
				v: 'John 3:30',
				t: 'He must increase, but I <em>must</em> decrease.'
			},
			{
				v: 'John 3:36',
				t: 'He that believeth on the Son hath everlasting life: and he that believeth not the Son shall not see life; but the wrath of God abideth on him.'
			},
			{
				v: 'John 4:24',
				t: '<span class="red">God <em>is</em> a Spirit: and they that worship <em>him</em> must worship him in spirit and in truth.</span>'
			},
			{
				v: 'John 5:24',
				t: '<span class="red">Verily, verily, I say unto you, He that heareth my word, and believeth on him that sent me, hath everlasting life, and shall not come into condemnation; but is passed from death unto life.</span>'
			},
			{
				v: 'John 6:13',
				t: '<span class="red">Howbeit when he, the Spirit of truth, is come, he will guide you into all truth: for he shall not speak of himself; but whatsoever he shall hear, <em>that</em> shall he speak: and he will shew you things to come.</span>'
			},
			{
				v: 'John 6:29',
				t: 'Jesus answered and said unto them, <span class="red">This is the work of God, that ye believe on him whom he hath sent.</span>'
			},
			{
				v: 'John 6:35',
				t: 'And Jesus said unto them, <span class="red">I am the bread of life: he that cometh to me shall never hunger; and he that believeth on me shall never thirst.</span>'
			},
			{
				v: 'John 6:38',
				t: '<span class="red">For I came down from heaven, not to do mine own will, but the will of him that sent me.</span>'
			},
			{
				v: 'John 6:40',
				t: '<span class="red">And this is the will of him that sent me, that every one which seeth the Son, and believeth on him, may have everlasting life: and I will raise him up at the last day.</span>'
			},
			{
				v: 'John 6:47',
				t: '<span class="red">Verily, verily, I say unto you, He that believeth on me hath everlasting life.</span>'
			},
			{
				v: 'John 7:37-38',
				t: 'In the last day, that great <em>day</em> of the feast, Jesus stood and cried, saying, <span class="red">If any man thirst, let him come unto me, and drink. He that believeth on me, as the scripture hath said, out of his belly shall flow rivers of living water.</span>'
			},
			{
				v: 'John 8:12',
				t: 'Then spake Jesus again unto them, saying, <span class="red">I am the light of the world: he that followeth me shall not walk in darkness, but shall have the light of life.</span>'
			},
			{
				v: 'John 8:31-32',
				t: 'Then said Jesus to those Jews which believed on him, <span class="red">If ye continue in my word, <em>then</em> are ye my disciples indeed; And ye shall know the truth, and the truth shall make you free.</span>'
			},
			{
				v: 'John 8:24',
				t: '<span class="red">I said therefore unto you, that ye shall die in your sins: for if ye believe not that I am <em>he</em>, ye shall die in your sins.</span>'
			},
			{
				v: 'John 8:34-36',
				t: 'Jesus answered them, <span class="red">Verily, verily, I say unto you, Whosoever committeth sin is the servant of sin. And the servant abideth not in the house for ever: <em>but</em> the Son abideth ever. If the Son therefore shall make you free, ye shall be free indeed.</span>'
			},
			{
				v: 'John 8:44',
				t: '<span class="red">Ye are of <em>your</em> father the devil, and the lusts of your father ye will do. He was a murderer from the beginning, and abode not in the truth, because there is no truth in him. When he speaketh a lie, he speaketh of his own: for he is a liar, and the father of it.</span>'
			},
			{
				v: 'John 8:51',
				t: '<span class="red">Verily, verily, I say unto you, If a man keep my saying, he shall never see death.</span>'
			},
			{
				v: 'John 8:58',
				t: 'Jesus said unto them, <span class="red">Verily, verily, I say unto you, Before Abraham was, I am.</span>'
			},
			{
				v: 'John 10:9',
				t: '<span class="red">I am the door: by me if any man enter in, he shall be saved, and shall go in and out, and find pasture.</span>'
			},
			{
				v: 'John 10:10',
				t: '<span class="red">The thief cometh not, but for to steal, and to kill, and to destroy: I am come that they might have life, and that they might have <em>it</em> more abundantly.</span>'
			},
			{
				v: 'John 10:11',
				t: '<span class="red">I am the good shepherd: the good shepherd giveth his life for the sheep.</span>'
			},
			{
				v: 'John 10:14',
				t: '<span class="red">I am the good shepherd, and know my <em>sheep</em>, and am known of mine.</span>'
			},
			{
				v: 'John 10:11',
				t: '<span class="red">I am the good shepherd: the good shepherd giveth his life for the sheep.</span>'
			},
			{
				v: 'John 10:27-28',
				t: '<span class="red">My sheep hear my voice, and I know them, and they follow me: And I give unto them eternal life; and they shall never perish, neither shall any <em>man</em> pluck them out of my hand.</span>'
			},
			{
				v: 'John 10:30',
				t: '<span class="red">I and <em>my</em> Father are one.</span>'
			},
			{
				v: 'John 11:25',
				t: 'Jesus said unto her, <span class="red">I am the resurrection, and the life: he that believeth in me, though he were dead, yet shall he live:</span>'
			},
			{
				v: 'John 11:26',
				t: '<span class="red">And whosoever liveth and believeth in me shall never die. Believest thou this?</span>'
			},
			{
				v: 'John 13:34',
				t: '<span class="red">A new commandment I give unto you, That ye love one another; as I have loved you, that ye also love one another.</span>'
			},
			{
				v: 'John 12:46',
				t: '<span class="red">I am come a light into the world, that whosoever believeth on me should not abide in darkness.</span>'
			},
			{
				v: 'John 14:1',
				t: '<span class="red">Let not your heart be troubled: ye believe in God, believe also in me.</span>'
			},
			{
				v: 'John 14:6',
				t: 'Jesus saith unto him, <span class="red">I am the way, the truth, and the life: no man cometh unto the Father, but by me.</span>'
			},
			{
				v: 'John 14:15',
				t: '<span class="red">If ye love me, keep my commandments.</span>'
			},
			{
				v: 'John 14:16',
				t: '<span class="red">And I will pray the Father, and he shall give you another Comforter, that he may abide with you for ever;</span>'
			},
			{
				v: 'John 14:20',
				t: '<span class="red">At that day ye shall know that I <em>am</em> in my Father, and ye in me, and I in you.</span>'
			},
			{
				v: 'John 14:21',
				t: '<span class="red">He that hath my commandments, and keepeth them, he it is that loveth me: and he that loveth me shall be loved of my Father, and I will love him, and will manifest myself to him.</span>'
			},
			{
				v: 'John 14:23',
				t: 'Jesus answered and said unto him, <span class="red">If a man love me, he will keep my words: and my Father will love him, and we will come unto him, and make our abode with him.</span>'
			},
			{
				v: 'John 14:26',
				t: '<span class="red">But the Comforter, <em>which is</em> the Holy Ghost, whom the Father will send in my name, he shall teach you all things, and bring all things to your remembrance, whatsoever I have said unto you.</span>'
			},
			{
				v: 'John 14:27',
				t: '<span class="red">Peace I leave with you, my peace I give unto you: not as the world giveth, give I unto you. Let not your heart be troubled, neither let it be afraid.</span>'
			},
			{
				v: 'John 15:1-2',
				t: '<span class="red">I am the true vine, and my Father is the husbandman. Every branch in me that beareth not fruit he taketh away: and every <em>branch</em> that beareth fruit, he purgeth it, that it may bring forth more fruit.</span>'
			},
			{
				v: 'John 15:5',
				t: '<span class="red">I am the vine, ye <em>are</em> the branches: He that abideth in me, and I in him, the same bringeth forth much fruit: for without me ye can do nothing.</span>'
			},
			{
				v: 'John 15:6',
				t: '<span class="red">If a man abide not in me, he is cast forth as a branch, and is withered; and men gather them, and cast <em>them</em> into the fire, and they are burned.</span>'
			},
			{
				v: 'John 15:7',
				t: '<span class="red">If ye abide in me, and my words abide in you, ye shall ask what ye will, and it shall be done unto you.</span>'
			},
			{
				v: 'John 15:9',
				t: '<span class="red">As the Father hath loved me, so have I loved you: continue ye in my love.</span>'
			},
			{
				v: 'John 15:10',
				t: '<span class="red">If ye keep my commandments, ye shall abide in my love; even as I have kept my Father\'s commandments, and abide in his love.</span>'
			},
			{
				v: 'John 15:12',
				t: '<span class="red">This is my commandment, That ye love one another, as I have loved you.</span>'
			},
			{
				v: 'John 15:13',
				t: '<span class="red">Greater love hath no man than this, that a man lay down his life for his friends.</span>'
			},
			{
				v: 'John 15:16',
				t: '<span class="red">Ye have not chosen me, but I have chosen you, and ordained you, that ye should go and bring forth fruit, and <em>that</em> your fruit should remain: that whatsoever ye shall ask of the Father in my name, he may give it you.</span>'
			},
			{
				v: 'John 15:17',
				t: '<span class="red">These things I command you, that ye love one another.</span>'
			},
			{
				v: 'John 15:18',
				t: '<span class="red">If the world hate you, ye know that it hated me before <em>it hated</em> you.</span>'
			},
			{
				v: 'John 15:19',
				t: '<span class="red">If ye were of the world, the world would love his own: but because ye are not of the world, but I have chosen you out of the world, therefore the world hateth you.</span>'
			},
			{
				v: 'John 15:20',
				t: '<span class="red">Remember the word that I said unto you, The servant is not greater than his lord. If they have persecuted me, they will also persecute you; if they have kept my saying, they will keep yours also.</span>'
			},
			{
				v: 'John 15:23',
				t: '<span class="red">He that hateth me hateth my Father also.</span>'
			},
			{
				v: 'John 16:33',
				t: '<span class="red">These things I have spoken unto you, that in me ye might have peace. In the world ye shall have tribulation: but be of good cheer; I have overcome the world.</span>'
			},
			{
				v: 'John 17:3',
				t: '<span class="red">And this is life eternal, that they might know thee the only true God, and Jesus Christ, whom thou hast sent.</span>'
			},
			{
				v: 'John 18:20',
				t: 'Jesus answered him, <span class="red">I spake openly to the world; I ever taught in the synagogue, and in the temple, whither the Jews always resort; and in secret have I said nothing.</span>'
			},
			{
				v: 'John 19:30',
				t: 'When Jesus therefore had received the vinegar, he said, <span class="red">It is finished:</span> and he bowed his head, and gave up the ghost.'
			},
			{
				v: 'John 20:31',
				t: 'But these are written, that ye might believe that Jesus is the Christ, the Son of God; and that believing ye might have life through his name.'
			},
			// Acts
			{
				v: 'Acts 1:8',
				t: '<span class="red">But ye shall receive power, after that the Holy Ghost is come upon you: and ye shall be witnesses unto me both in Jerusalem, and in all Judaea, and in Samaria, and unto the uttermost part of the earth.</span>'
			},
			{
				v: 'Acts 2:38',
				t: 'Then Peter said unto them, Repent, and be baptized every one of you in the name of Jesus Christ for the remission of sins, and ye shall receive the gift of the Holy Ghost.'
			},
			{
				v: 'Acts 3:19',
				t: 'Repent ye therefore, and be converted, that your sins may be blotted out, when the times of refreshing shall come from the presence of the Lord;'
			},
			{
				v: 'Acts 4:11-12',
				t: 'This is the stone which was set at nought of you builders, which is become the head of the corner. Neither is there salvation in any other: for there is none other name under heaven given among men, whereby we must be saved.'
			},
			{
				v: 'Acts 5:29',
				t: 'Then Peter and the <em>other</em> apostles answered and said, We ought to obey God rather than men.'
			},
			{
				v: 'Acts 5:31',
				t: 'Him hath God exalted with his right hand <em>to be</em> a Prince and a Saviour, for to give repentance to Israel, and forgiveness of sins.'
			},
			{
				v: 'Acts 10:43',
				t: 'To him give all the prophets witness, that through his name whosoever believeth in him shall receive remission of sins.'
			},
			{
				v: 'Acts 14:22',
				t: 'Confirming the souls of the disciples, <em>and</em> exhorting them to continue in the faith, and that we must through much tribulation enter into the kingdom of God.'
			},
			{
				v: 'Acts 20:28',
				t: 'Take heed therefore unto yourselves, and to all the flock, over the which the Holy Ghost hath made you overseers, to feed the church of God, which he hath purchased with his own blood.'
			},
			{
				v: 'Acts 20:29',
				t: 'For I know this, that after my departing shall grievous wolves enter in among you, not sparing the flock.'
			},
			{
				v: 'Acts 20:35',
				t: 'I have shewed you all things, how that so labouring ye ought to support the weak, and to remember the words of the Lord Jesus, how he said, <span class="red">It is more blessed to give than to receive.</span>'
			},
			// Romans
			{
				v: 'Romans 1:16-17',
				t: 'For I am not ashamed of the gospel of Christ: for it is the power of God unto salvation to every one that believeth; to the Jew first, and also to the Greek. For therein is the righteousness of God revealed from faith to faith: as it is written, The just shall live by faith.'
			},
			{
				v: 'Romans 1:20',
				t: 'For the invisible things of him from the creation of the world are clearly seen, being understood by the things that are made, <em>even</em> his eternal power and Godhead; so that they are without excuse:'
			},
			{
				v: 'Romans 1:22',
				t: 'Professing themselves to be wise, they became fools,'
			},
			{
				v: 'Romans 1:24-25',
				t: 'Wherefore God also gave them up to uncleanness through the lusts of their own hearts, to dishonour their own bodies between themselves: Who changed the truth of God into a lie, and worshipped and served the creature more than the Creator, who is blessed for ever. Amen.'
			},
			{
				v: 'Romans 1:28',
				t: 'And even as they did not like to retain God in <em>their</em> knowledge, God gave them over to a reprobate mind, to do those things which are not convenient;'
			},
			{
				v: 'Romans 2:11',
				t: 'For there is no respect of persons with God.'
			},
			{
				v: 'Romans 3:10',
				t: 'As it is written, There is none righteous, no, not one:'
			},
			{
				v: 'Romans 3:12',
				t: 'They are all gone out of the way, they are together become unprofitable; there is none that doeth good, no, not one.'
			},
			{
				v: 'Romans 3:23-24',
				t: 'For all have sinned, and come short of the glory of God; Being justified freely by his grace through the redemption that is in Christ Jesus:'
			},
			{
				v: 'Romans 3:28',
				t: 'Therefore we conclude that a man is justified by faith without the deeds of the law.'
			},
			{
				v: 'Romans 5:1-2',
				t: 'Therefore being justified by faith, we have peace with God through our Lord Jesus Christ: By whom also we have access by faith into this grace wherein we stand, and rejoice in hope of the glory of God.'
			},
			{
				v: 'Romans 5:8',
				t: 'But God commendeth his love toward us, in that, while we were yet sinners, Christ died for us.'
			},
			{
				v: 'Romans 5:9',
				t: 'Much more then, being now justified by his blood, we shall be saved from wrath through him.'
			},
			{
				v: 'Romans 5:10',
				t: 'For if, when we were enemies, we were reconciled to God by the death of his Son, much more, being reconciled, we shall be saved by his life.'
			},
			{
				v: 'Romans 5:12',
				t: 'Wherefore, as by one man sin entered into the world, and death by sin; and so death passed upon all men, for that all have sinned:'
			},
			{
				v: 'Romans 5:18',
				t: 'Therefore as by the offence of one <em>judgment came</em> upon all men to condemnation; even so by the righteousness of one <em>the free gift came</em> upon all men unto justification of life.'
			},
			{
				v: 'Romans 6:14',
				t: 'For sin shall not have dominion over you: for ye are not under the law, but under grace.'
			},
			{
				v: 'Romans 6:23',
				t: 'For the wages of sin <em>is</em> death; but the gift of God <em>is</em> eternal life through Jesus Christ our Lord.'
			},
			{
				v: 'Romans 7:6',
				t: 'But now we are delivered from the law, that being dead wherein we were held; that we should serve in newness of spirit, and not <em>in</em> the oldness of the letter.'
			},
			{
				v: 'Romans 8:1',
				t: '<em>There is</em> therefore now no condemnation to them which are in Christ Jesus, who walk not after the flesh, but after the Spirit.'
			},
			{
				v: 'Romans 8:28',
				t: 'And we know that all things work together for good to them that love God, to them who are the called according to <em>his</em> purpose.'
			},
			{
				v: 'Romans 8:31',
				t: 'What shall we then say to these things? If God <em>be</em> for us, who <em>can be</em> against us?'
			},
			{
				v: 'Romans 8:34',
				t: 'Who <em>is</em> he that condemneth? <em>It is</em> Christ that died, yea rather, that is risen again, who is even at the right hand of God, who also maketh intercession for us.'
			},
			{
				v: 'Romans 8:38-39',
				t: 'For I am persuaded, that neither death, nor life, nor angels, nor principalities, nor powers, nor things present, nor things to come, Nor height, nor depth, nor any other creature, shall be able to separate us from the love of God, which is in Christ Jesus our Lord.'
			},
			{
				v: 'Romans 9:15',
				t: 'For he saith to Moses, I will have mercy on whom I will have mercy, and I will have compassion on whom I will have compassion.'
			},
			{
				v: 'Romans 10:9',
				t: 'That if thou shalt confess with thy mouth the Lord Jesus, and shalt believe in thine heart that God hath raised him from the dead, thou shalt be saved.'
			},
			{
				v: 'Romans 10:13',
				t: 'For whosoever shall call upon the name of the Lord shall be saved.'
			},
			{
				v: 'Romans 11:36',
				t: 'For of him, and through him, and to him, <em>are</em> all things: to whom <em>be</em> glory for ever. Amen.'
			},
			{
				v: 'Romans 12:1-2',
				t: 'I beseech you therefore, brethren, by the mercies of God, that ye present your bodies a living sacrifice, holy, acceptable unto God, <em>which is</em> your reasonable service. And be not conformed to this world: but be ye transformed by the renewing of your mind, that ye may prove what <em>is</em> that good, and acceptable, and perfect, will of God.'
			},
			{
				v: 'Romans 12:14',
				t: 'Bless them which persecute you: bless, and curse not.'
			},
			{
				v: 'Romans 12:17-18',
				t: 'Recompense to no man evil for evil. Provide things honest in the sight of all men. If it be possible, as much as lieth in you, live peaceably with all men.'
			},
			{
				v: 'Romans 12:19',
				t: 'Dearly beloved, avenge not yourselves, but <em>rather</em> give place unto wrath: for it is written, Vengeance <em>is</em> mine; I will repay, saith the Lord.'
			},
			{
				v: 'Romans 12:21',
				t: 'Be not overcome of evil, but overcome evil with good.'
			},
			{
				v: 'Romans 13:8',
				t: 'Owe no man any thing, but to love one another: for he that loveth another hath fulfilled the law.'
			},
			{
				v: 'Romans 13:10',
				t: 'Love worketh no ill to his neighbour: therefore love <em>is</em> the fulfilling of the law.'
			},
			{
				v: 'Romans 14:11',
				t: 'For it is written, <em>As</em> I live, saith the Lord, every knee shall bow to me, and every tongue shall confess to God.'
			},
			{
				v: 'Romans 15:13',
				t: 'Now the God of hope fill you with all joy and peace in believing, that ye may abound in hope, through the power of the Holy Ghost.'
			},
			{
				v: 'Romans 16:17-18',
				t: 'Now I beseech you, brethren, mark them which cause divisions and offences contrary to the doctrine which ye have learned; and avoid them. For they that are such serve not our Lord Jesus Christ, but their own belly; and by good words and fair speeches deceive the hearts of the simple.'
			},
			// 1 Corinthians
			{
				v: '1 Corinthians 1:18',
				t: 'For the preaching of the cross is to them that perish foolishness; but unto us which are saved it is the power of God.'
			},
			{
				v: '1 Corinthians 1:27',
				t: 'But God hath chosen the foolish things of the world to confound the wise; and God hath chosen the weak things of the world to confound the things which are mighty;'
			},
			{
				v: '1 Corinthians 2:9',
				t: 'But as it is written, Eye hath not seen, nor ear heard, neither have entered into the heart of man, the things which God hath prepared for them that love him.'
			},
			{
				v: '1 Corinthians 2:11',
				t: 'For what man knoweth the things of a man, save the spirit of man which is in him? even so the things of God knoweth no man, but the Spirit of God.'
			},
			{
				v: '1 Corinthians 2:12',
				t: 'Now we have received, not the spirit of the world, but the spirit which is of God; that we might know the things that are freely given to us of God.'
			},
			{
				v: '1 Corinthians 2:13',
				t: 'Which things also we speak, not in the words which man\'s wisdom teacheth, but which the Holy Ghost teacheth; comparing spiritual things with spiritual.'
			},
			{
				v: '1 Corinthians 2:14',
				t: 'But the natural man receiveth not the things of the Spirit of God: for they are foolishness unto him: neither can he know <em>them</em>, because they are spiritually discerned.'
			},
			{
				v: '1 Corinthians 3:17',
				t: 'If any man defile the temple of God, him shall God destroy; for the temple of God is holy, which <em>temple</em> ye are.'
			},
			{
				v: '1 Corinthians 6:9-10',
				t: 'Know ye not that the unrighteous shall not inherit the kingdom of God? Be not deceived: neither fornicators, nor idolaters, nor adulterers, nor effeminate, nor abusers of themselves with mankind, Nor thieves, nor covetous, nor drunkards, nor revilers, nor extortioners, shall inherit the kingdom of God.'
			},
			{
				v: '1 Corinthians 6:20',
				t: 'For ye are bought with a price: therefore glorify God in your body, and in your spirit, which are God\'s.'
			},
			{
				v: '1 Corinthians 7:23',
				t: 'Ye are bought with a price; be not ye the servants of men.'
			},
			{
				v: '1 Corinthians 8:6',
				t: 'But to us <em>there is but</em> one God, the Father, of whom <em>are</em> all things, and we in him; and one Lord Jesus Christ, by whom <em>are</em> all things, and we by him.'
			},
			{
				v: '1 Corinthians 10:14',
				t: 'Wherefore, my dearly beloved, flee from idolatry.'
			},
			{
				v: '1 Corinthians 12:3',
				t: 'Wherefore I give you to understand, that no man speaking by the Spirit of God calleth Jesus accursed: and <em>that</em> no man can say that Jesus is the Lord, but by the Holy Ghost.'
			},
			{
				v: '1 Corinthians 13:4',
				t: 'Charity suffereth long, <em>and</em> is kind; charity envieth not; charity vaunteth not itself, is not puffed up,'
			},
			{
				v: '1 Corinthians 13:11',
				t: 'When I was a child, I spake as a child, I understood as a child, I thought as a child: but when I became a man, I put away childish things.'
			},
			{
				v: '1 Corinthians 13:13',
				t: 'And now abideth faith, hope, charity, these three; but the greatest of these <em>is</em> charity.'
			},
			{
				v: '1 Corinthians 14:33',
				t: 'For God is not <em>the author</em> of confusion, but of peace, as in all churches of the saints.'
			},
			{
				v: '1 Corinthians 15:10',
				t: 'But by the grace of God I am what I am: and his grace which <em>was bestowed</em> upon me was not in vain; but I laboured more abundantly than they all: yet not I, but the grace of God which was with me.'
			},
			{
				v: '1 Corinthians 15:57',
				t: 'But thanks <em>be</em> to God, which giveth us the victory through our Lord Jesus Christ.'
			},
			{
				v: '1 Corinthians 16:13-14',
				t: 'Watch ye, stand fast in the faith, quit you like men, be strong. Let all your things be done with charity.'
			},
			// 2 Corinthians
			{
				v: '2 Corinthians 3:17',
				t: 'Now the Lord is that Spirit: and where the Spirit of the Lord <em>is</em>, there <em>is</em> liberty.'
			},
			{
				v: '2 Corinthians 4:3-4',
				t: 'But if our gospel be hid, it is hid to them that are lost: In whom the god of this world hath blinded the minds of them which believe not, lest the light of the glorious gospel of Christ, who is the image of God, should shine unto them.'
			},
			{
				v: '2 Corinthians 4:5',
				t: 'For we preach not ourselves, but Christ Jesus the Lord; and ourselves your servants for Jesus\' sake.'
			},
			{
				v: '2 Corinthians 5:6-7',
				t: 'Therefore <em>we are</em> always confident, knowing that, whilst we are at home in the body, we are absent from the Lord: (For we walk by faith, not by sight:)'
			},
			{
				v: '2 Corinthians 5:17',
				t: 'Therefore if any man <em>be</em> in Christ, <em>he is</em> a new creature: old things are passed away; behold, all things are become new.'
			},
			{
				v: '2 Corinthians 5:21',
				t: 'For our sake he made him <em>to be</em> sin who knew no sin, so that in him we might become the righteousness of God.'
			},
			{
				v: '2 Corinthians 6:14',
				t: 'Be ye not unequally yoked together with unbelievers: for what fellowship hath righteousness with unrighteousness? and what communion hath light with darkness?'
			},
			{
				v: '2 Corinthians 9:6',
				t: 'But this <em>I say</em>, He which soweth sparingly shall reap also sparingly; and he which soweth bountifully shall reap also bountifully.'
			},
			// Galatians
			{
				v: 'Galatians 1:10',
				t: 'For do I now persuade men, or God? or do I seek to please men? for if I yet pleased men, I should not be the servant of Christ.'
			},
			{
				v: 'Galatians 2:16',
				t: 'Knowing that a man is not justified by the works of the law, but by the faith of Jesus Christ, even we have believed in Jesus Christ, that we might be justified by the faith of Christ, and not by the works of the law: for by the works of the law shall no flesh be justified.'
			},
			{
				v: 'Galatians 2:20',
				t: 'I am crucified with Christ: nevertheless I live; yet not I, but Christ liveth in me: and the life which I now live in the flesh I live by the faith of the Son of God, who loved me, and gave himself for me.'
			},
			{
				v: 'Galatians 2:21',
				t: 'I do not frustrate the grace of God: for if righteousness <em>come</em> by the law, then Christ is dead in vain.'
			},
			{
				v: 'Galatians 3:6',
				t: 'Even as Abraham believed God, and it was accounted to him for righteousness.'
			},
			{
				v: 'Galatians 3:22',
				t: 'But the scripture hath concluded all under sin, that the promise by faith of Jesus Christ might be given to them that believe.'
			},
			{
				v: 'Galatians 3:26',
				t: 'For ye are all the children of God by faith in Christ Jesus.'
			},
			{
				v: 'Galatians 3:28-29',
				t: 'There is neither Jew nor Greek, there is neither bond nor free, there is neither male nor female: for ye are all one in Christ Jesus. And if ye <em>be</em> Christ\'s, then are ye Abraham\'s seed, and heirs according to the promise.'
			},
			{
				v: 'Galatians 5:1',
				t: 'Stand fast therefore in the liberty wherewith Christ hath made us free, and be not entangled again with the yoke of bondage.'
			},
			{
				v: 'Galatians 5:9',
				t: 'A little leaven leaveneth the whole lump.'
			},
			{
				v: 'Galatians 5:13',
				t: 'For, brethren, ye have been called unto liberty; only <em>use</em> not liberty for an occasion to the flesh, but by love serve one another.'
			},
			{
				v: 'Galatians 5:14',
				t: 'For all the law is fulfilled in one word, <em>even</em> in this; Thou shalt love thy neighbour as thyself.'
			},
			{
				v: 'Galatians 5:16-17',
				t: '<em>This</em> I say then, Walk in the Spirit, and ye shall not fulfil the lust of the flesh. For the flesh lusteth against the Spirit, and the Spirit against the flesh: and these are contrary the one to the other: so that ye cannot do the things that ye would.'
			},
			{
				v: 'Galatians 5:22',
				t: 'But the fruit of the Spirit is love, joy, peace, longsuffering, gentleness, goodness, faith,'
			},
			{
				v: 'Galatians 6:7-8',
				t: 'Be not deceived; God is not mocked: for whatsoever a man soweth, that shall he also reap. For he that soweth to his flesh shall of the flesh reap corruption; but he that soweth to the Spirit shall of the Spirit reap life everlasting.'
			},
			{
				v: 'Galatians 6:9',
				t: 'And let us not be weary in well doing: for in due season we shall reap, if we faint not.'
			},
			{
				v: 'Galatians 6:10',
				t: 'As we have therefore opportunity, let us do good unto all <em>men</em>, especially unto them who are of the household of faith.'
			},
			// Ephesians
			{
				v: 'Ephesians 2:8-9',
				t: 'For by grace are ye saved through faith; and that not of yourselves: <em>it is</em> the gift of God: Not of works, lest any man should boast.'
			},
			{
				v: 'Ephesians 2:10',
				t: 'For we are his workmanship, created in Christ Jesus unto good works, which God hath before ordained that we should walk in them.'
			},
			{
				v: 'Ephesians 2:13',
				t: 'But now in Christ Jesus ye who sometimes were far off are made nigh by the blood of Christ.'
			},
			{
				v: 'Ephesians 4:29',
				t: 'Let no corrupt communication proceed out of your mouth, but that which is good to the use of edifying, that it may minister grace unto the hearers.'
			},
			{
				v: 'Ephesians 4:32',
				t: 'And be ye kind one to another, tenderhearted, forgiving one another, even as God for Christ\'s sake hath forgiven you.'
			},
			{
				v: 'Ephesians 5:5',
				t: 'For this ye know, that no whoremonger, nor unclean person, nor covetous man, who is an idolater, hath any inheritance in the kingdom of Christ and of God.'
			},
			{
				v: 'Ephesians 5:11',
				t: 'And have no fellowship with the unfruitful works of darkness, but rather reprove <em>them</em>.'
			},
			{
				v: 'Ephesians 5:22-23',
				t: 'Wives, submit yourselves unto your own husbands, as unto the Lord. For the husband is the head of the wife, even as Christ is the head of the church: and he is the saviour of the body.'
			},
			{
				v: 'Ephesians 5:25',
				t: 'Husbands, love your wives, even as Christ also loved the church, and gave himself for it;'
			},
			{
				v: 'Ephesians 6:10',
				t: 'Finally, my brethren, be strong in the Lord, and in the power of his might.'
			},
			{
				v: 'Ephesians 6:12',
				t: 'For we wrestle not against flesh and blood, but against principalities, against powers, against the rulers of the darkness of this world, against spiritual wickedness in high <em>places</em>.'
			},
			{
				v: 'Ephesians 6:13',
				t: 'Wherefore take unto you the whole armour of God, that ye may be able to withstand in the evil day, and having done all, to stand.'
			},
			// Philippians
			{
				v: 'Philippians 1:6',
				t: 'Being confident of this very thing, that he which hath begun a good work in you will perform <em>it</em> until the day of Jesus Christ:'
			},
			{
				v: 'Philippians 2:8',
				t: 'And being found in fashion as a man, he humbled himself, and became obedient unto death, even the death of the cross.'
			},
			{
				v: 'Philippians 2:9-10',
				t: 'Wherefore God also hath highly exalted him, and given him a name which is above every name: That at the name of Jesus every knee should bow, of <em>things</em> in heaven, and <em>things</em> in earth, and <em>things</em> under the earth;'
			},
			{
				v: 'Philippians 4:4-5',
				t: 'Rejoice in the Lord always: <em>and</em> again I say, Rejoice. Let your moderation be known unto all men. The Lord <em>is</em> at hand.'
			},
			{
				v: 'Philippians 4:6',
				t: 'Be careful for nothing; but in every thing by prayer and supplication with thanksgiving let your requests be made known unto God.'
			},
			{
				v: 'Philippians 4:13',
				t: 'I can do all things through Christ which strengtheneth me.'
			},
			// Colossians
			{
				v: 'Colossians 1:14',
				t: 'In whom we have redemption through his blood, <em>even</em> the forgiveness of sins:'
			},
			{
				v: 'Colossians 1:15',
				t: 'Who is the image of the invisible God, the firstborn of every creature:'
			},
			{
				v: 'Colossians 1:18',
				t: 'And he is the head of the body, the church: who is the beginning, the firstborn from the dead; that in all <em>things</em> he might have the preeminence.'
			},
			{
				v: 'Colossians 2:8',
				t: 'Beware lest any man spoil you through philosophy and vain deceit, after the tradition of men, after the rudiments of the world, and not after Christ.'
			},
			{
				v: 'Colossians 3:2',
				t: 'Set your affection on things above, not on things on the earth.'
			},
			{
				v: 'Colossians 3:12',
				t: 'Put on therefore, as the elect of God, holy and beloved, bowels of mercies, kindness, humbleness of mind, meekness, longsuffering;'
			},
			{
				v: 'Colossians 3:13',
				t: 'Forbearing one another, and forgiving one another, if any man have a quarrel against any: even as Christ forgave you, so also <em>do</em> ye.'
			},
			// 1 Thessalonians
			{
				v: '1 Thessalonians 4:3-5',
				t: 'For this is the will of God, <em>even</em> your sanctification, that ye should abstain from fornication: That every one of you should know how to possess his vessel in sanctification and honour; Not in the lust of concupiscence, even as the Gentiles which know not God:'
			},
			{
				v: '1 Thessalonians 5:16-18',
				t: 'Rejoice evermore. Pray without ceasing. In every thing give thanks: for this is the will of God in Christ Jesus concerning you.'
			},
			{
				v: '1 Thessalonians 5:21',
				t: 'Prove all things; hold fast that which is good.'
			},
			// 2 Thessalonians
			{
				v: '2 Thessalonians 2:3',
				t: 'Let no man deceive you by any means: for <em>that day shall not come</em>, except there come a falling away first, and that man of sin be revealed, the son of perdition;'
			},
			{
				v: '2 Thessalonians 3:3',
				t: 'But the Lord is faithful, who shall stablish you, and keep <em>you</em> from evil.'
			},
			// 1 Timothy
			{
				v: '1 Timothy 2:5',
				t: 'For <em>there is</em> one God, and one mediator between God and men, the man Christ Jesus;'
			},
			{
				v: '1 Timothy 2:11-12',
				t: 'Let the woman learn in silence with all subjection. But I suffer not a woman to teach, nor to usurp authority over the man, but to be in silence.'
			},
			{
				v: '1 Timothy 3:16',
				t: 'And without controversy great is the mystery of godliness: God was manifest in the flesh, justified in the Spirit, seen of angels, preached unto the Gentiles, believed on in the world, received up into glory.'
			},
			{
				v: '1 Timothy 4:15',
				t: 'Meditate upon these things; give thyself wholly to them; that thy profiting may appear to all.'
			},
			{
				v: '1 Timothy 6:7',
				t: 'For we brought nothing into <em>this</em> world, <em>and it is</em> certain we can carry nothing out.'
			},
			{
				v: '1 Timothy 6:9-10',
				t: 'But they that will be rich fall into temptation and a snare, and <em>into</em> many foolish and hurtful lusts, which drown men in destruction and perdition. For the love of money is the root of all evil: which while some coveted after, they have erred from the faith, and pierced themselves through with many sorrows.'
			},
			// 2 Timothy
			{
				v: '2 Timothy 1:7',
				t: 'For God hath not given us the spirit of fear; but of power, and of love, and of a sound mind.'
			},
			{
				v: '2 Timothy 1:13',
				t: 'Hold fast the form of sound words, which thou hast heard of me, in faith and love which is in Christ Jesus.'
			},
			{
				v: '2 Timothy 2:15',
				t: 'Study to shew thyself approved unto God, a workman that needeth not to be ashamed, rightly dividing the word of truth.'
			},
			{
				v: '2 Timothy 3:12',
				t: 'Yea, and all that will live godly in Christ Jesus shall suffer persecution.'
			},
			{
				v: '2 Timothy 3:16-17',
				t: 'All scripture <em>is</em> given by inspiration of God, and <em>is</em> profitable for doctrine, for reproof, for correction, for instruction in righteousness: That the man of God may be perfect, throughly furnished unto all good works.'
			},
			{
				v: '2 Timothy 4:7',
				t: 'I have fought a good fight, I have finished <em>my</em> course, I have kept the faith:'
			},
			{
				v: '2 Timothy 4:18',
				t: 'And the Lord shall deliver me from every evil work, and will preserve <em>me</em> unto his heavenly kingdom: to whom <em>be</em> glory for ever and ever. Amen.'
			},
			// Titus
			{
				v: 'Titus 2:11-12',
				t: 'For the grace of God that bringeth salvation hath appeared to all men, Teaching us that, denying ungodliness and worldly lusts, we should live soberly, righteously, and godly, in this present world;'
			},
			{
				v: 'Titus 3:5',
				t: 'Not by works of righteousness which we have done, but according to his mercy he saved us, by the washing of regeneration, and renewing of the Holy Ghost;'
			},
			// Hebrews
			{
				v: 'Hebrews 4:16',
				t: 'Let us therefore come boldly unto the throne of grace, that we may obtain mercy, and find grace to help in time of need.'
			},
			{
				v: 'Hebrews 5:9',
				t: 'And being made perfect, he became the author of eternal salvation unto all them that obey him;'
			},
			{
				v: 'Hebrews 6:10',
				t: 'For God <em>is</em> not unrighteous to forget your work and labour of love, which ye have shewed toward his name, in that ye have ministered to the saints, and do minister.'
			},
			{
				v: 'Hebrews 7:27',
				t: 'Who needeth not daily, as those high priests, to offer up sacrifice, first for his own sins, and then for the people\'s: for this he did once, when he offered up himself.'
			},
			{
				v: 'Hebrews 9:12',
				t: 'Neither by the blood of goats and calves, but by his own blood he entered in once into the holy place, having obtained eternal redemption <em>for us</em>.'
			},
			{
				v: 'Hebrews 9:26',
				t: 'For then must he often have suffered since the foundation of the world: but now once in the end of the world hath he appeared to put away sin by the sacrifice of himself.'
			},
			{
				v: 'Hebrews 9:28',
				t: 'So Christ was once offered to bear the sins of many; and unto them that look for him shall he appear the second time without sin unto salvation.'
			},
			{
				v: 'Hebrews 10:10',
				t: 'By the which will we are sanctified through the offering of the body of Jesus Christ once <em>for all</em>.'
			},
			{
				v: 'Hebrews 10:12',
				t: 'But this man, after he had offered one sacrifice for sins for ever, sat down on the right hand of God;'
			},
			{
				v: 'Hebrews 11:1',
				t: 'Now faith is the substance of things hoped for, the evidence of things not seen.'
			},
			{
				v: 'Hebrews 11:6',
				t: 'But without faith <em>it is</em> impossible to please <em>him</em>: for he that cometh to God must believe that he is, and <em>that</em> he is a rewarder of them that diligently seek him.'
			},
			{
				v: 'Hebrews 12:14',
				t: 'Follow peace with all <em>men</em>, and holiness, without which no man shall see the Lord:'
			},
			{
				v: 'Hebrews 13:5',
				t: '<em>Let your</em> conversation <em>be</em> without covetousness; <em>and be</em> content with such things as ye have: for he hath said, I will never leave thee, nor forsake thee.'
			},
			{
				v: 'Hebrews 13:8',
				t: 'Jesus Christ the same yesterday, and to day, and for ever.'
			},
			// James
			{
				v: 'James 1:5',
				t: 'If any of you lack wisdom, let him ask of God, that giveth to all <em>men</em> liberally, and upbraideth not; and it shall be given him.'
			},
			{
				v: 'James 1:12',
				t: 'Blessed <em>is</em> the man that endureth temptation: for when he is tried, he shall receive the crown of life, which the Lord hath promised to them that love him.'
			},
			{
				v: 'James 1:13',
				t: 'Let no man say when he is tempted, I am tempted of God: for God cannot be tempted with evil, neither tempteth he any man:'
			},
			{
				v: 'James 1:17',
				t: 'Every good gift and every perfect gift is from above, and cometh down from the Father of lights, with whom is no variableness, neither shadow of turning.'
			},
			{
				v: 'James 1:19',
				t: 'Wherefore, my beloved brethren, let every man be swift to hear, slow to speak, slow to wrath:'
			},
			{
				v: 'James 1:22',
				t: 'But be ye doers of the word, and not hearers only, deceiving your own selves.'
			},
			{
				v: 'James 1:27',
				t: 'Pure religion and undefiled before God and the Father is this, To visit the fatherless and widows in their affliction, <em>and</em> to keep himself unspotted from the world.'
			},
			{
				v: 'James 2:18',
				t: 'Yea, a man may say, Thou hast faith, and I have works: shew me thy faith without thy works, and I will shew thee my faith by my works.'
			},
			{
				v: 'James 2:23',
				t: 'And the scripture was fulfilled which saith, Abraham believed God, and it was imputed unto him for righteousness: and he was called the Friend of God.'
			},
			{
				v: 'James 2:26',
				t: 'For as the body without the spirit is dead, so faith without works is dead also.'
			},
			{
				v: 'James 4:3',
				t: 'Ye ask, and receive not, because ye ask amiss, that ye may consume <em>it</em> upon your lusts.'
			},
			{
				v: 'James 4:4',
				t: 'Ye adulterers and adulteresses, know ye not that the friendship of the world is enmity with God? whosoever therefore will be a friend of the world is the enemy of God.'
			},
			{
				v: 'James 4:6',
				t: 'But he giveth more grace. Wherefore he saith, God resisteth the proud, but giveth grace unto the humble.'
			},
			{
				v: 'James 4:7',
				t: 'Submit yourselves therefore to God. Resist the devil, and he will flee from you.'
			},
			{
				v: 'James 4:8',
				t: 'Draw nigh to God, and he will draw nigh to you. Cleanse <em>your</em> hands, <em>ye</em> sinners; and purify <em>your</em> hearts, <em>ye</em> double minded.'
			},
			{
				v: 'James 5:13',
				t: 'Is any among you afflicted? let him pray. Is any merry? let him sing psalms.'
			},
			{
				v: 'James 5:16',
				t: 'Confess <em>your</em> faults one to another, and pray one for another, that ye may be healed. The effectual fervent prayer of a righteous man availeth much.'
			},
			// 1 Peter
			{
				v: '1 Peter 1:15-16',
				t: 'But as he which hath called you is holy, so be ye holy in all manner of conversation; Because it is written, Be ye holy; for I am holy.'
			},
			{
				v: '1 Peter 2:24',
				t: 'Who his own self bare our sins in his own body on the tree, that we, being dead to sins, should live unto righteousness: by whose stripes ye were healed.'
			},
			{
				v: '1 Peter 3:9',
				t: 'Not rendering evil for evil, or railing for railing: but contrariwise blessing; knowing that ye are thereunto called, that ye should inherit a blessing.'
			},
			{
				v: '1 Peter 3:18',
				t: 'For Christ also hath once suffered for sins, the just for the unjust, that he might bring us to God, being put to death in the flesh, but quickened by the Spirit:'
			},
			{
				v: '1 Peter 4:11',
				t: 'If any man speak, <em>let him speak</em> as the oracles of God; if any man minister, <em>let him do it</em> as of the ability which God giveth: that God in all things may be glorified through Jesus Christ, to whom be praise and dominion for ever and ever. Amen.'
			},
			{
				v: '1 Peter 5:6-7',
				t: 'Humble yourselves therefore under the mighty hand of God, that he may exalt you in due time: Casting all your care upon him; for he careth for you.'
			},
			{
				v: '1 Peter 5:8',
				t: 'Be sober, be vigilant; because your adversary the devil, as a roaring lion, walketh about, seeking whom he may devour:'
			},
			// 2 Peter
			{
				v: '2 Peter 3:9',
				t: 'The Lord is not slack concerning his promise, as some men count slackness; but is longsuffering to us-ward, not willing that any should perish, but that all should come to repentance.'
			},
			// 1 John
			{
				v: '1 John 1:7',
				t: 'But if we walk in the light, as he is in the light, we have fellowship one with another, and the blood of Jesus Christ his Son cleanseth us from all sin.'
			},
			{
				v: '1 John 1:9',
				t: 'If we confess our sins, he is faithful and just to forgive us <em>our</em> sins, and to cleanse us from all unrighteousness.'
			},
			{
				v: '1 John 2:15',
				t: 'Love not the world, neither the things <em>that are</em> in the world. If any man love the world, the love of the Father is not in him.'
			},
			{
				v: '1 John 2:17',
				t: 'And the world passeth away, and the lust thereof: but he that doeth the will of God abideth for ever.'
			},
			{
				v: '1 John 2:18',
				t: 'Little children, it is the last time: and as ye have heard that antichrist shall come, even now are there many antichrists; whereby we know that it is the last time.'
			},
			{
				v: '1 John 3:1',
				t: 'Behold, what manner of love the Father hath bestowed upon us, that we should be called the sons of God: therefore the world knoweth us not, because it knew him not.'
			},
			{
				v: '1 John 3:8',
				t: 'He that committeth sin is of the devil; for the devil sinneth from the beginning. For this purpose the Son of God was manifested, that he might destroy the works of the devil.'
			},
			{
				v: '1 John 3:16',
				t: 'Hereby perceive we the love <em>of God</em>, because he laid down his life for us: and we ought to lay down <em>our</em> lives for the brethren.'
			},
			{
				v: '1 John 4:1',
				t: 'Beloved, believe not every spirit, but try the spirits whether they are of God: because many false prophets are gone out into the world.'
			},
			{
				v: '1 John 4:4',
				t: 'Ye are of God, little children, and have overcome them: because greater is he that is in you, than he that is in the world.'
			},
			{
				v: '1 John 4:8',
				t: 'He that loveth not knoweth not God; for God is love.'
			},
			{
				v: '1 John 4:15',
				t: 'Whosoever shall confess that Jesus is the Son of God, God dwelleth in him, and he in God.'
			},
			{
				v: '1 John 4:18',
				t: 'There is no fear in love; but perfect love casteth out fear: because fear hath torment. He that feareth is not made perfect in love.'
			},
			{
				v: '1 John 4:19',
				t: 'We love him, because he first loved us.'
			},
			{
				v: '1 John 5:13',
				t: 'These things have I written unto you that believe on the name of the Son of God; that ye may know that ye have eternal life, and that ye may believe on the name of the Son of God.'
			},
			// Jude
			{
				v: 'Jude 1:24-25',
				t: 'Now unto him that is able to keep you from falling, and to present <em>you</em> faultless before the presence of his glory with exceeding joy, To the only wise God our Saviour, <em>be</em> glory and majesty, dominion and power, both now and ever. Amen.'
			},
			// Revelation
			{
				v: 'Revelation 1:5-6',
				t: 'And from Jesus Christ, <em>who is</em> the faithful witness, <em>and</em> the first begotten of the dead, and the prince of the kings of the earth. Unto him that loved us, and washed us from our sins in his own blood, And hath made us kings and priests unto God and his Father; to him <em>be</em> glory and dominion for ever and ever. Amen.'
			},
			{
				v: 'Revelation 1:8',
				t: '<span class="red">I am Alpha and Omega, the beginning and the ending</span>, saith the Lord, which is, and which was, and which is to come, the Almighty.'
			},
			{
				v: 'Revelation 3:20',
				t: '<span class="red">Behold, I stand at the door, and knock: if any man hear my voice, and open the door, I will come in to him, and will sup with him, and he with me.</span>'
			},
			{
				v: 'Revelation 4:11',
				t: 'Thou art worthy, O Lord, to receive glory and honour and power: for thou hast created all things, and for thy pleasure they are and were created.'
			},
			{
				v: 'Revelation 5:12',
				t: 'Saying with a loud voice, Worthy is the Lamb that was slain to receive power, and riches, and wisdom, and strength, and honour, and glory, and blessing.'
			},
			{
				v: 'Revelation 15:4',
				t: 'Who shall not fear thee, O Lord, and glorify thy name? for <em>thou</em> only <em>art</em> holy: for all nations shall come and worship before thee; for thy judgments are made manifest.'
			},
			{
				v: 'Revelation 17:4-5',
				t: 'And the woman was arrayed in purple and scarlet colour, and decked with gold and precious stones and pearls, having a golden cup in her hand full of abominations and filthiness of her fornication: And upon her forehead <em>was</em> a name written, <span class="small-caps">Mystery, Babylon The Great, The Mother Of Harlots And Abominations Of The Earth</span>.'
			},
			{
				v: 'Revelation 17:14',
				t: 'These shall make war with the Lamb, and the Lamb shall overcome them: for he is Lord of lords, and King of kings: and they that are with him <em>are</em> called, and chosen, and faithful.'
			},
			{
				v: 'Revelation 21:4',
				t: 'And God shall wipe away all tears from their eyes; and there shall be no more death, neither sorrow, nor crying, neither shall there be any more pain: for the former things are passed away.'
			},
			{
				v: 'Revelation 21:8',
				t: 'But the fearful, and unbelieving, and the abominable, and murderers, and whoremongers, and sorcerers, and idolaters, and all liars, shall have their part in the lake which burneth with fire and brimstone: which is the second death.'
			},
			{
				v: 'Revelation 22:12',
				t: '<span class="red">And, behold, I come quickly; and my reward <em>is</em> with me, to give every man according as his work shall be.</span>'
			},
			{
				v: 'Revelation 22:13',
				t: '<span class="red">I am Alpha and Omega, the beginning and the end, the first and the last.</span>'
			},
		];

	const PROPERTIES = {
		order: 'random',
		redLetter: true
	};

	let urlParams = new URL(window.location.toLocaleString()).searchParams,
		spaceKeyDown = false;

	function getVerse() {
		let arrLen = BIBLE_VERSES.length,
			verse;
		if (!arrLen) {
			// If BIBLE_VERSES is empty, reload the page.
			location.reload();
		} else if (PROPERTIES.order == 'sequential') {
			verse = BIBLE_VERSES[0];
			BIBLE_VERSES.splice(0, 1);
		} else { 
			// The default order is "random"
			let randNum = Math.floor(Math.random() * arrLen);
			verse = BIBLE_VERSES[randNum];
			BIBLE_VERSES.splice(randNum, 1);
		}
		if (PROPERTIES.redLetter) {
			document.getElementById('text').innerHTML = verse.t;
		} else {
			document.getElementById('text').innerHTML = verse.t.replace(/class="red"/g, '');
		}
		document.getElementById('verse').innerHTML = verse.v;
	}

	document.addEventListener('keydown', function(e) {
		if (e.code == 'Space' && !spaceKeyDown) {
			spaceKeyDown = true;
			getVerse();
		}
	}, false);

	document.addEventListener('keyup', function(e) {
		if (e.code == 'Space') {
			spaceKeyDown = false;
		}
	}, false);

	document.addEventListener('click', function() {
		getVerse();
	}, false);

	if (urlParams.get('order') == 'sequential') {
		PROPERTIES.order = 'sequential';
	}

	if (urlParams.get('red-letter') == 'off') {
		PROPERTIES.redLetter = false;
	}

	getVerse();

}, true);